package com.example.m08.nausjoc.app;

import com.badlogic.gdx.InputProcessor;

public interface InputChangeable {
	public void setInput(InputProcessor input);
}
