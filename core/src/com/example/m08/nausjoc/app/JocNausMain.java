package com.example.m08.nausjoc.app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Logger;
import com.example.m08.nausjoc.load.DefaultResources;
import com.example.m08.nausjoc.load.LoadInformeable;
import com.example.m08.nausjoc.load.LoadingScreen;
import com.example.m08.nausjoc.model.BulletModel;
import com.example.m08.nausjoc.model.PlayerShip;
import com.example.m08.nausjoc.parameters.Constants;
import com.example.m08.nausjoc.screen.AbstractGameScreen;
import com.example.m08.nausjoc.screen.AbstractScreen;
import com.example.m08.nausjoc.screen.FirstPhase;
import com.example.m08.nausjoc.screen.ScreenChangeable;
import com.example.m08.nausjoc.screen.StartMenu;
import com.example.m08.nausjoc.shapeutils.GroupedRectangleForms;
import com.example.m08.nausjoc.shapeutils.ShapeType;
import com.example.m08.nausjoc.utils.GameInfoPackage;

public class JocNausMain implements ApplicationListener, ScreenChangeable, InputChangeable, PlayerShipLoadeable {
	
	public static final String RESOURCE_INFO_PATH;
	private static final int SHIP_VELOCITY = 200;
	private static final int DEFAULT_REINFORCEMENTS = 50;
	
	static {
		RESOURCE_INFO_PATH = "resources_references/references.txt";
	}
	
	private Screen actualScreen;
	private GameInfoPackage infoPackage;
	
	private List<AbstractGameScreen> gameScreens;
	private Hashtable<String, AbstractScreen> screens;
	
	private Logger infoLog;
	private Logger errorLog;
	
	private PlayerShip playerShip;
	
	
	@Override
	public void create () {
		Gdx.app.setLogLevel(Application.LOG_INFO);
		screens = new Hashtable<String, AbstractScreen>();
		gameScreens = new ArrayList<AbstractGameScreen>();
		playerShip = null;
		//gameScreens = new TreeSet<AbstractGameScreen>();
		infoLog = new Logger("INFO", Application.LOG_INFO);
		errorLog = new Logger("ERROR", Application.LOG_ERROR);
		
		AssetManager manager = new AssetManager();
		InternalFileHandleResolver resolver = new InternalFileHandleResolver();
		manager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
		
		SpriteBatch batch = new SpriteBatch();
		OrthographicCamera camera = new OrthographicCamera();
		int screenWidth = Gdx.graphics.getWidth();
		int screenHeight = Gdx.graphics.getHeight();
		infoPackage = new GameInfoPackage(manager, batch, camera, screenWidth, screenHeight);
		
		//---------------------------------------
		final String menuId = "menu";
		final String firstPhaseId = "first";
		//---------------------------------------
		StartMenu menu = new StartMenu(menuId, infoPackage, this, infoLog, this, new String[]{firstPhaseId}, this);
		screens.put(menuId, menu);
		FirstPhase firstPhase = new FirstPhase(firstPhaseId, infoPackage, this, infoLog, new String[]{menuId});
		screens.put(firstPhaseId, firstPhase);
		gameScreens.add(firstPhase);
		//---------------------------------------
		
		LoadingScreen loadingScreen = new LoadingScreen(infoPackage, new LoadInformeable() {
			
			@Override
			public void onLoad() {
				changeScreen(menuId);
			}
			
			@Override
			public int getProgress() {
				infoPackage.manager.update();
				return (int)(infoPackage.manager.getProgress() * 100);
			}
			
			@Override
			public int getMaxiumProgress() {
				return 100;
			}
		}, infoLog);
		
		try {
			DefaultResources.loadAllResources(infoPackage.manager, Gdx.files.internal(RESOURCE_INFO_PATH));
		} catch (IOException e) {
			errorLog.error("Error en la lectura dels fitxers de recursos", e);
		}
		loadingScreen.show();
		actualScreen = loadingScreen;

	}

	@Override
	public void render () {
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        //Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		//Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		float delta = Gdx.graphics.getDeltaTime();
		actualScreen.render(delta);
		
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		infoLog.info("Tancant joc.");
		
	}
	
	@Override
	public void changeScreen(String screenId) {
		actualScreen.dispose();
		AbstractScreen screen = screens.get(screenId);
		
		//if (!screen.isLoaded()) {
		//	infoLog.info("Carregant pantalla.");
		//	LoadingScreen loadingScreen = new LoadingScreen(infoPackage, screen, infoLog);
		//	screen.loadAssets();
		//	actualScreen = loadingScreen;
		//} else {
		//infoLog.info("Pantalla carregada.");
		screen.show();
		actualScreen = screen;
		Gdx.input.setInputProcessor(screen.getInputProcessor());
			
	//	}
	}
	
	@Override
	public void exitGame() {
		Gdx.app.exit();
	}

	@Override
	public void setInput(InputProcessor input) {
		Gdx.input.setInputProcessor(input);
	}

	@Override
	public void loadPlayerShip() {
		
		if (playerShip == null) {
			String pathAtlasShip = DefaultResources.getPathByAlias(Constants.ATLAS_SHIP);
			String pathAtlasReinf = DefaultResources.getPathByAlias(Constants.ATLAS_REINF);
			String pathLineBulletFirs = DefaultResources.getPathByAlias(Constants.LINE_BULLET1);
			String pathLineBulletSeco = DefaultResources.getPathByAlias(Constants.LINE_BULLET2);
			
			TextureAtlas playerAtlas = infoPackage.manager.get(pathAtlasShip, TextureAtlas.class);
			TextureAtlas reinforcementAtlas = infoPackage.manager.get(pathAtlasReinf, TextureAtlas.class);
			
			Texture linearBullet1 = infoPackage.manager.get(pathLineBulletFirs, Texture.class);
			Texture linearBullet2 = infoPackage.manager.get(pathLineBulletSeco, Texture.class);
			
			List<Rectangle> rectangleList = new ArrayList<Rectangle>();
			Rectangle rectLeft = new Rectangle(20, 0, 20, 80);
			Rectangle rectCenter = new Rectangle(40, 0, 20, 160);
			Rectangle rectRight = new Rectangle(60, 0, 20, 80);
			rectangleList.add(rectLeft);
			rectangleList.add(rectCenter);
			rectangleList.add(rectRight);
			/*float[] verts = new float[] {
					rectLeft.x, rectLeft.y, 
					rectLeft.x, rectLeft.y + rectLeft.height, 
					rectCenter.x, rectLeft.y + rectLeft.height, 
					rectCenter.x, rectCenter.y + rectCenter.height,
					rectCenter.x + rectCenter.width, rectCenter.y + rectCenter.height,
					rectRight.x, rectRight.y + rectRight.height,
					rectRight.x + rectRight.width, rectRight.y + rectRight.height,
					rectRight.x + rectRight.width, rectRight.y,
					rectLeft.x, rectLeft.y };*/
			GroupedRectangleForms formsPlayer = new GroupedRectangleForms(20, 0, rectangleList, true);
			Animation anim = new Animation(0.1f, playerAtlas.getRegions());
			Animation reinfAnim = new Animation(0.1f, reinforcementAtlas.getRegions());
			
			BulletModel bullShip = new BulletModel(linearBullet1, 200, 20, 60, ShapeType.RECTANGLE);
			BulletModel bullReinf = new BulletModel(linearBullet2, 10, 10, 30, ShapeType.RECTANGLE);
			int x = 0;
			int y = 0;
			int width = 120;
			int height = 160;
			int life = 3000;
			int reinforcementLife = 500;
			//int reinfBaseDamage = 50;
			float reinfDistanceFromCenter = 120;
			playerShip = new PlayerShip(playerAtlas.getTextures().first(), x, y, width, height, 
					life, reinforcementLife/*, reinfBaseDamage*/, reinfDistanceFromCenter,
					SHIP_VELOCITY, anim, reinfAnim, 
					formsPlayer, bullShip, bullReinf, infoPackage.screenWidth, 
					infoPackage.screenHeight);
			playerShip.setDifferenceOfSprite(20, 20, 0, 0);
			/*for (int i = 0; i < DEFAULT_REINFORCEMENTS; i++) {
				playerShip.addNewReinforcement();
			}*/
			for (AbstractGameScreen gameScreen : gameScreens) {
				gameScreen.setPlayerShip(playerShip);
			}
		}
	}
	
	
}
