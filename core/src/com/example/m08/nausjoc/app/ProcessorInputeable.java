package com.example.m08.nausjoc.app;

import com.badlogic.gdx.InputProcessor;

public interface ProcessorInputeable {
	public InputProcessor getInputProcessor();
}
