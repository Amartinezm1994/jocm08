package com.example.m08.nausjoc.parameters;

public class Constants {
	public static final String ATLAS_SHIP = "ATLAS_SHIP";
	public static final String ATLAS_REINF = "ATLAS_REINF";
	public static final String TEXT_BOSS1 = "TEXT_BOSS1";
	public static final String TEXT_BOSS2 = "TEXT_BOSS2";
	public static final String TEXT_BOSS3 = "TEXT_BOSS3";
	public static final String MUSIC_FACING = "MUSIC_FACING";
	public static final String MUSIC_AMUNRA = "MUSIC_AMUNRA";
	public static final String MUSIC_WANDERER = "MUSIC_WANDERER";
	public static final String CIRC_BULLET1 = "CIRC_BULLET1";
	public static final String CIRC_BULLET2 = "CIRC_BULLET2";
	public static final String CIRC_BULLET3 = "CIRC_BULLET3";
	public static final String CIRC_BULLET4 = "CIRC_BULLET4";
	public static final String LINE_BULLET1 = "LINE_BULLET1";
	public static final String LINE_BULLET2 = "LINE_BULLET2";
}
