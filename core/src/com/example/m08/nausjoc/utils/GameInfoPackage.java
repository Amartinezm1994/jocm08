package com.example.m08.nausjoc.utils;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameInfoPackage {
	
	public final AssetManager manager;
	public final SpriteBatch batch;
	public final OrthographicCamera camera;
	
	public final int screenWidth;
	public final int screenHeight;
	
	public GameInfoPackage(AssetManager manager, 
			SpriteBatch batch, OrthographicCamera camera,
			int screenWidth, int screenHeight) {
		this.manager = manager;
		this.batch = batch;
		this.camera = camera;
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
	}
}
