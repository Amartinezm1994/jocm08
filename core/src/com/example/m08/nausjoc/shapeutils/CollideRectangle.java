package com.example.m08.nausjoc.shapeutils;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

public class CollideRectangle implements ShapeCollideable {
	
	private static final ShapeType TYPE = ShapeType.RECTANGLE;
	
	private Rectangle rectangle;
	
	/**
	 * Needs to be normalized, the x and y is the center of shape,
	 * don't use the sprite reference.
	 * @param rectangle
	 */
	public CollideRectangle(Rectangle rectangle/*, Polygon polygon*/) {
		//super(getPolygonRectangle(rectangle), TYPE);
		this.rectangle = rectangle;
	}
	/**
	 * Use the reference of sprite, not the normal shape.
	 * @param x
	 * @param y
	 * @param radius
	 */
	public CollideRectangle(float x, float y, float width, float height) {
		//this(new Rectangle(x + width / 2, y + height / 2, width / 2, height / 2));
		this(new Rectangle(x, y, width, height));
	}
	/*public CollideRectangle(Rectangle rectangle, float[] verts) {
		super(getPolygonRectangle(rectangle), TYPE);
		this.rectangle = rectangle;
	}*/
	
	
	@Override
	public boolean collide(ShapeCollideable collideable) {
		switch(collideable.getType()) {
		case RECTANGLE:
			return rectangle.overlaps(((CollideRectangle)collideable).getRectangle());
		case CIRCLE:
			return Intersector.overlaps((((CollideCircle)collideable).getCircle()), rectangle);
		case GROUPED_RECTANGLE:
			return ((GroupedRectangleForms)collideable).overlapsRectangle(rectangle);
		}
		//return super.collide(collideable);
		return false;
	}
	
	public Rectangle getRectangle() {
		return rectangle;
	}
	@Override
	public void setPosition(float x, float y) {
		rectangle.x = x/* + rectangle.width*/;
		rectangle.y = y/* + rectangle.height*/;		
	}
	@Override
	public ShapeType getType() {
		return TYPE;
	}

	/*@Override
	public void move(float x, float y) {
		rectangle.x += x;
		rectangle.y += y;
		super.move(x, y);
	}*/
	
	/*private static Polygon getPolygonRectangle(Rectangle rectangle) {
		return new Polygon(new float[] {
				rectangle.x, rectangle.y, 
				rectangle.x + rectangle.width, rectangle.y,
				rectangle.x + rectangle.width, rectangle.y + rectangle.height,
				rectangle.x, rectangle.y});
	}*/


	
}
