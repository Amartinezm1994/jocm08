package com.example.m08.nausjoc.shapeutils;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;

public class CollideCircle implements ShapeCollideable {
	
	private static final ShapeType TYPE = ShapeType.CIRCLE;
	//private static final int DEFAULT_VERTICE_POINTS = 10;
	
	private Circle circle;
	
	/**
	 * Needs to be normalized, the x and y is the center of shape,
	 * don't use the sprite reference.
	 * @param rectangle
	 */
	public CollideCircle(Circle circle) {
		//super(getCirclePolygon(circle, DEFAULT_VERTICE_POINTS), TYPE);
		this.circle = circle;
	}
	/**
	 * Use the reference of sprite, not the normal shape.
	 * @param x
	 * @param y
	 * @param radius
	 */
	public CollideCircle(float x, float y, float radius) {
		this(new Circle(x + radius / 2, y + radius / 2, radius / 2));
	}
	

	@Override
	public boolean collide(ShapeCollideable collideable) {
		switch(collideable.getType()) {
		case CIRCLE:
			return circle.overlaps(((CollideCircle)collideable).getCircle());
		case RECTANGLE:
			return Intersector.overlaps(circle, (((CollideRectangle)collideable).getRectangle()));
		case GROUPED_RECTANGLE:
			return ((GroupedRectangleForms)collideable).overlapsCircle(circle);
		}
		return false;
	}
	
	public Circle getCircle() {
		return circle;
	}
	
	@Override
	public void setPosition(float x, float y) {
		circle.x = x + circle.radius;
		circle.y = y + circle.radius;
		//super.setPosition(x, y);
	}

	@Override
	public ShapeType getType() {
		return TYPE;
	}

	/*private static Polygon getCirclePolygon(Circle circle, int numPoints) {
		double perimeter = circle.radius * 2 * Math.PI;
		double angularVelo = 2 * Math.PI / perimeter;
		//System.out.println(perimeter + " " + angularVelo);
		float[] vertices = new float[numPoints * 2];
		//System.out.println("---");
		for (int i = 0; i < numPoints * 2; i += 2) {
			double angularMoment = ((double)i / (numPoints * 2)) * (angularVelo * perimeter);
			vertices[i] = (float)(circle.radius * Math.cos(angularMoment));
			vertices[i + 1] = (float)(circle.radius * Math.sin(angularMoment));
			
			//System.out.println(vertices[i] + " " + vertices[i + 1] + " " + angularMoment);
		}
		//System.out.println("---");
		return new Polygon(vertices);
	}*/
	
	

	/*@Override
	public void move(float x, float y) {
		circle.x += x;
		circle.y += y;
		super.move(x, y);
	}*/

	
}
