package com.example.m08.nausjoc.shapeutils;

import java.util.List;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

public class GroupedRectangleForms implements ShapeCollideable {
	
	private static final ShapeType TYPE = ShapeType.GROUPED_RECTANGLE;
	
	private List<Rectangle>	rectangles;
	private float refX;
	private float refY;
	
	public GroupedRectangleForms(float refX, float refY, 
			List<Rectangle> rectangles, boolean autoNormalize/*, Polygon polygon*/) {
		//super(polygon, TYPE);
		this.refX = refX;
		this.refY = refY;
		if (autoNormalize) {
			for (Rectangle rect : rectangles) {
				rect.width /= 2;
				rect.height /= 2;
				rect.x = rect.x + rect.width;
				rect.y = rect.y + rect.height;
			}
		}
		this.rectangles = rectangles;
		
	}
	/*public GroupedRectangleForms(List<Rectangle> rectangles, float[] verts) {
		this(rectangles, new Polygon(verts));
	}*/
	
	/*public GroupedRectangleForms(float[] vertices) {
		rectangles = new ArrayList<Rectangle>();
		Rectangle rect;
		for (int i = 0; i < vertices.length; i++) {
			rect.set
		}
	}
	*/
	
	/*public void move(float x, float y) {
		for (Rectangle rect : rectangles) {
			rect.x += x;
			rect.y += y;
		}
		super.move(x, y);
	}
	*/
	@Override
	public boolean collide(ShapeCollideable collideable) {
		switch(collideable.getType()) {
		case RECTANGLE:
			return overlapsRectangle(((CollideRectangle)collideable).getRectangle());
		case CIRCLE:
			return overlapsCircle((((CollideCircle)collideable).getCircle()));
		case GROUPED_RECTANGLE:
			List<Rectangle> otherGroup = ((GroupedRectangleForms)collideable).getRectangles();
			for (Rectangle rect : rectangles) {
				for (Rectangle oRect : otherGroup) {
					if (rect.overlaps(oRect)) {
						return true;
					}
				}
			}
			return false;
		}
		/*if (collideable.getType() == TYPE) {
			List<Rectangle> otherGroup = ((GroupedRectangleForms)collideable).getRectangles();
			for (Rectangle rect : rectangles) {
				for (Rectangle oRect : otherGroup) {
					if (rect.overlaps(oRect)) {
						return true;
					}
				}
			}
		}*/
		
		return false;
	}

	/*@Override
	public Polygon getPolygon() {
		float[] totalRect = new float[rectangles.size() * 8];
		int indexTotalRect = 0;
		for (Rectangle rect : rectangles) {
			totalRect[indexTotalRect++] = rect.x;
			totalRect[indexTotalRect++] = rect.y;
			totalRect[indexTotalRect++] = rect.x + rect.width;
			totalRect[indexTotalRect++] = rect.y;
			totalRect[indexTotalRect++] = rect.x + rect.width;
			totalRect[indexTotalRect++] = rect.y + rect.height;
			totalRect[indexTotalRect++] = rect.x;
			totalRect[indexTotalRect++] = rect.y + rect.height;
		}
		return new Polygon(totalRect);
	}*/
	
	public boolean overlapsCircle(Circle circle) {
		//List<Rectangle> rectList = ((GroupedRectangleForms)collideable).getRectangles();
		for (Rectangle rect : rectangles) {
			if (Intersector.overlaps(circle, rect)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean overlapsRectangle(Rectangle rectangle) {
		//List<Rectangle> rectList = ((GroupedRectangleForms)collideable).getRectangles();
		for (Rectangle rect : rectangles) {
			if (rectangle.overlaps(rect)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public ShapeType getType() {
		return TYPE;
	}
	
	public List<Rectangle> getRectangles() {
		return rectangles;
	}
	@Override
	public void setPosition(float x, float y) {
		
		//System.out.println("--------------");
		for (Rectangle rect : rectangles) {
			rect.x = (rect.x - rect.width - refX) + x + rect.width;
			//rect.x = (rect.x - refX) + x;
			rect.y = (rect.y - rect.height - refY) + y + rect.height;
			//rect.y = (rect.y - refY) + y;
			//System.out.println(rect.x + " " + rect.y);
		}
		refX = x;
		refY = y;
	}
	
	//public static boolean overlapsG
}
