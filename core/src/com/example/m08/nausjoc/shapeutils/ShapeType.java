package com.example.m08.nausjoc.shapeutils;

public enum ShapeType {
	CIRCLE, RECTANGLE, GROUPED_RECTANGLE
}
