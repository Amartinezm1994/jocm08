package com.example.m08.nausjoc.shapeutils;

import com.badlogic.gdx.math.Polygon;

public interface ShapeCollideable {
	public boolean collide(ShapeCollideable collideable);
	//public Polygon getPolygon();
	public ShapeType getType();
	//public Polygon getPolygon();
	//public void move(float x, float y);
	public void setPosition(float x, float y);
}
