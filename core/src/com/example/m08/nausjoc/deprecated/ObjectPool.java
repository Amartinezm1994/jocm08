package com.example.m08.nausjoc.deprecated;

import java.util.Enumeration;
import java.util.Hashtable;

//http://www.javaworld.com/article/2076690/java-concurrency/build-your-own-objectpool-in-java-to-boost-app-speed.html
public abstract class ObjectPool<T> {
	
	public static final long DEFAULT_EXPIRATION_TIME = 30000l;
	
	private long expireTime;
	private Hashtable<T, Long> locked, unlocked;
	
	public ObjectPool(long expireTime) {
		this.expireTime = expireTime;
		locked = new Hashtable<T, Long>();
		unlocked = new Hashtable<T, Long>();
	}
	public ObjectPool() {
		this(DEFAULT_EXPIRATION_TIME);
	}
	
	public abstract T create();
	public abstract boolean validate(T object);
	public abstract void expire(T object);
	
	public synchronized T checkOut() {
		if (unlocked.size() > 0) {
			Enumeration<T> enumer = unlocked.keys();
			while(enumer.hasMoreElements()) {
				T obj = enumer.nextElement();
				if (now() - unlocked.get(obj) > expireTime) {
					unlocked.remove(obj);
					expire(obj);
				} else {
					if (validate(obj)) {
						unlocked.remove(obj);
						locked.put(obj, now());
						return obj;
					} else {
						unlocked.remove(obj);
						expire(obj);
					}
				}
			}
		}
		T obj = create();
		locked.put(obj, now());
		return obj;
	}
	public synchronized void checkIn(T object) {
		locked.remove(object);
		unlocked.put(object, now());
	}
	
	public static long now() {
		return System.currentTimeMillis();
	}
}
