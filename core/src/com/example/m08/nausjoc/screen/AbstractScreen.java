package com.example.m08.nausjoc.screen;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.utils.Logger;
import com.example.m08.nausjoc.app.ProcessorInputeable;
import com.example.m08.nausjoc.utils.GameInfoPackage;

public abstract class AbstractScreen implements Screen, ProcessorInputeable {
	
	protected GameInfoPackage infoPack;
	protected String screenId;
	protected ScreenChangeable changeable;
	protected Logger infoLog;
	//protected InputChangeable inpChange;
	protected String[] availablePhases;
	
	public AbstractScreen(String screenId, GameInfoPackage infoPack, ScreenChangeable changeable, 
			Logger infoLog/*, InputChangeable inpChange*/, String[] availablePhases) {
		this.screenId = screenId;
		this.infoPack = infoPack;
		this.changeable = changeable;
		this.infoLog = infoLog;
		//this.inpChange = inpChange;
		this.availablePhases = availablePhases;
	}
	
	public String getId() {
		return screenId;
	}
	
	//public abstract boolean isLoaded();
	//public abstract void loadAssets();
	//public abstract void buildScreen();
	
}
