package com.example.m08.nausjoc.screen;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Logger;
import com.example.m08.nausjoc.app.InputChangeable;
import com.example.m08.nausjoc.app.PlayerShipLoadeable;
import com.example.m08.nausjoc.load.DefaultResources;
import com.example.m08.nausjoc.utils.GameInfoPackage;

public class StartMenu extends AbstractScreen {
	
	private static final String SKIN_JSON;
	private static final String BG_TEXTURE;
	private static final String BG_MUSIC;
	private static final String DEF_FONT;
	private static final String TEXT_FONT;
	
	static {
		SKIN_JSON = "SKIN_JSON";
		BG_TEXTURE = "BG_FIRST";
		BG_MUSIC = "MUSIC_RUBICON";
		DEF_FONT = "FONT_DEFAULT";
		TEXT_FONT = "FONT_TEXT";
	}
	
	private Stage firstStage;
	private List<Stage> stageList;
	private Stage activeStage;
	
	private TextButton button;
	private TextButton buttonExit;

	private Texture background;
	private Skin uiSkin;
	private Music bgMusic;
	private BitmapFont defaultFont;
	private BitmapFont textFont;
	
	
	private PlayerShipLoadeable shipLoadeable;
	
	
	private InputChangeable inpChange;
	
	public StartMenu(String screenId, GameInfoPackage infoPackage, ScreenChangeable screenChangeable, 
			Logger infoLog, InputChangeable inpChange, String[] gamePhases, PlayerShipLoadeable shipLoadeable) {
		super(screenId, infoPackage, screenChangeable, infoLog/*, inpChange*/, gamePhases);
		this.inpChange = inpChange;
		
		//secondStage = new Stage();
		
		this.shipLoadeable = shipLoadeable;
		//rendering = false;
	}

	@Override
	public void show() {
		String pathJson = DefaultResources.getPathByAlias(SKIN_JSON);
		String pathBackground = DefaultResources.getPathByAlias(BG_TEXTURE);
		String pathBgMusic = DefaultResources.getPathByAlias(BG_MUSIC);
		String pathFont = DefaultResources.getPathByAlias(DEF_FONT);
		String pathTextFont = DefaultResources.getPathByAlias(TEXT_FONT);
		
		
		
		FreeTypeFontGenerator newFont = infoPack.manager.get(pathFont);
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 80;
		parameter.color = Color.CYAN;
		parameter.borderWidth = 3;
		parameter.borderColor = Color.WHITE;
		defaultFont = newFont.generateFont(parameter);
		
		FreeTypeFontGenerator secondFont = infoPack.manager.get(pathTextFont);
		FreeTypeFontParameter parameterSecond = new FreeTypeFontParameter();
		parameterSecond.size = 30;
		parameterSecond.borderWidth = 1;
		parameterSecond.borderColor = Color.GRAY;
		textFont = secondFont.generateFont(parameterSecond);
		
		bgMusic = infoPack.manager.get(pathBgMusic);
		bgMusic.setVolume(0.5f);
		bgMusic.setLooping(true);
		
		
		uiSkin = infoPack.manager.get(pathJson);
		
		background = infoPack.manager.get(pathBackground);
		
		stageList = new ArrayList<Stage>();
		firstStage = new Stage();
		
		button = new TextButton("Comenzar", uiSkin);
		button.setWidth(200f);
		button.setHeight(70f);
		button.setPosition(infoPack.screenWidth / 2 - button.getWidth() / 2, infoPack.screenHeight / 2 + 15);
		firstStage.addActor(button);
		button.addListener(new ButtonStartListener());
		
		buttonExit = new TextButton("Salida", uiSkin);
		buttonExit.setWidth(200f);
		buttonExit.setHeight(70f);
		buttonExit.setPosition(infoPack.screenWidth / 2 - button.getWidth() / 2, infoPack.screenHeight / 2 - buttonExit.getHeight() - 15);
		firstStage.addActor(buttonExit);
		buttonExit.addListener(new ButtonExitListener());
		
		activeStage = firstStage;
		
		Stage newStage = null;
		for (int i =  0; i < availablePhases.length; i++) {
			if (i % 4 == 0) {
				newStage = new Stage();
				stageList.add(newStage);
				TextButton leftButton = new TextButton("Atras", uiSkin);
				leftButton.setWidth(80f);
				leftButton.setHeight(50f);
				leftButton.setPosition(infoPack.screenWidth / 10, infoPack.screenHeight / 50);
				newStage.addActor(leftButton);
				if (i == 0) {
					leftButton.addListener(new FirstButtonAdvanceListener());
				} else {
					leftButton.addListener(new ButtonAdvanceListener(i / 4 - 1));
				}
				if ((availablePhases.length - i) > 4) {
					TextButton rightButton = new TextButton("Adelante", uiSkin);
					rightButton.setWidth(80f);
					rightButton.setHeight(50f);
					rightButton.setPosition(infoPack.screenWidth - infoPack.screenWidth / 10 - rightButton.getWidth(), infoPack.screenHeight / 50);
					newStage.addActor(rightButton);
					rightButton.addListener(new ButtonAdvanceListener((i + 4) / 4));
					//System.out.println(i / 4);
				}
			}
			TextButton newButton = new TextButton(i + ": " + availablePhases[i], uiSkin);
			newButton.setWidth(200f);
			newButton.setHeight(70f);
			newButton.setPosition(infoPack.screenWidth / 2 - newButton.getWidth() / 2, 
					infoPack.screenHeight / 6f * (4 - (i % 4f)));
			newButton.addListener(new GenericButtonListener(availablePhases[i]));
			newStage.addActor(newButton);
			//infoLog.info(i + " carregada.");
		}
		
		bgMusic.play();
	}

	@Override
	public void render(float delta) {
		//if (rendering) {
		//Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		
		infoPack.batch.begin();
		infoPack.batch.draw(background, 0, 0, infoPack.screenWidth, infoPack.screenHeight);
		if (activeStage == firstStage) {
			defaultFont.draw(infoPack.batch, "Walpur", 
					infoPack.screenWidth / 2f - defaultFont.getBounds("Walpur").width / 2f, 
					infoPack.screenHeight / 3f * 2.5f);
		} else {
			textFont.draw(infoPack.batch, "Selecciona pantalla", 
					infoPack.screenWidth / 2f - textFont.getBounds("Selecciona pantalla").width / 2f, 
					infoPack.screenHeight / 5f * 4.5f);
		}
		infoPack.batch.end();
		activeStage.act(delta);
		activeStage.draw();
		
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		bgMusic.pause();
	}

	@Override
	public void resume() {
		bgMusic.play();
	}

	@Override
	public void hide() {
		bgMusic.pause();
	}

	@Override
	public void dispose() {
		bgMusic.dispose();
		bgMusic = null;
		//background.dispose();
		background = null;
		firstStage.dispose();
		firstStage = null;
		//uiSkin.dispose();
		uiSkin = null;
		defaultFont.dispose();
		defaultFont = null;
		textFont.dispose();
		textFont = null;
		for (Stage stage : stageList) {
			stage.dispose();
		}
		stageList = null;
		//firstStage.dispose();
	}
	
	@Override
	public InputProcessor getInputProcessor() {
		return activeStage;
	}
	
	

	/*@Override
	public int getProgress() {
		return assetsLoaded;
	}

	@Override
	public int getMaxiumProgress() {
		return totalAssetsToLoad;
	}*/

	/*@Override
	public boolean isLoaded() {
		return assetsLoaded >= totalAssetsToLoad && interfaceBuilded;
	}*/

	/*@Override
	public void loadAssets() {
		
		
		
		
	}
	
	@Override
	public void onLoad() {
		if (!interfaceBuilded) {
			infoLog.info("Construïnt interfície.");
			buildScreen();
			changeable.changeScreen(screenId);
		}
	}
	
	@Override
	public void buildScreen() {
		
		interfaceBuilded = true;
		
	}*/
	
	private void setStage(Stage stageToChange) {
		activeStage = stageToChange;
		inpChange.setInput(activeStage);
	}
	
	private class FirstButtonAdvanceListener extends ClickListener {
		
		@Override
		public void clicked(InputEvent event, float x, float y) {
			super.clicked(event, x, y);
			setStage(firstStage);
		}
	}
	
	private class ButtonAdvanceListener extends ClickListener {
		
		private int stageIndex;
		
		public ButtonAdvanceListener(int stageIndex) {
			this.stageIndex = stageIndex;
		}
		
		@Override
		public void clicked(InputEvent event, float x, float y) {
			super.clicked(event, x, y);
			//activeStage = stageList.get(stageIndex);
			setStage(stageList.get(stageIndex));
		}
	}
	
	private class GenericButtonListener extends ClickListener {
		
		private String phaseId;
		
		public GenericButtonListener(String phaseId) {
			this.phaseId = phaseId;
		}
		
		@Override
		public void clicked(InputEvent event, float x, float y) {
			super.clicked(event, x, y);
			infoLog.info("Creant nau del jugador i carregant dependències.");
			shipLoadeable.loadPlayerShip();
			StartMenu.this.changeable.changeScreen(phaseId);
		}
	}
	
	private class ButtonStartListener extends ClickListener {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			super.clicked(event, x, y);
			setStage(stageList.get(0));
		}
	}
	
	private class ButtonExitListener extends ClickListener {
		
		@Override
		public void clicked(InputEvent event, float x, float y) {
			super.clicked(event, x, y);
			StartMenu.this.changeable.exitGame();
		}
		
	}

	
}
