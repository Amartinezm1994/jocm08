package com.example.m08.nausjoc.screen;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.utils.Logger;
import com.example.m08.nausjoc.boss.IBulleteable;
import com.example.m08.nausjoc.boss.IFinishGame;
import com.example.m08.nausjoc.load.DefaultResources;
import com.example.m08.nausjoc.model.Bullet;
import com.example.m08.nausjoc.model.BulletPool;
import com.example.m08.nausjoc.model.ICollisionHealtheable;
import com.example.m08.nausjoc.model.MovableShip;
import com.example.m08.nausjoc.model.NoLifeException;
import com.example.m08.nausjoc.model.PlayerShip;
import com.example.m08.nausjoc.shapeutils.ShapeCollideable;
import com.example.m08.nausjoc.utils.GameInfoPackage;

public abstract class AbstractGameScreen extends AbstractScreen implements IBulleteable, IFinishGame {
	
	protected PlayerShip player;
	protected List<Bullet> shipBullets;
	protected List<Bullet> enemyBullets;
	protected List<ICollisionHealtheable> enemyCollisionable;
	protected BulletPool poolBullet;
	protected InputProcessor processor;
	protected BitmapFont textFont;
	
	private static final String TEXT_FONT = "FONT_TEXT";
	
	private boolean spacePressed;
	
	private float timeToCheck;
	private float currentTime;
	
	protected static final int MARGIN_PHASE = 50;

	public AbstractGameScreen(String screenId, GameInfoPackage infoPack,
			ScreenChangeable changeable, Logger infoLog,
			String[] availablePhases) {
		super(screenId, infoPack, changeable, infoLog, availablePhases);
		timeToCheck = 0.2f;
		enemyCollisionable = new ArrayList<ICollisionHealtheable>();
	}

	/*public void setPlayerShip(PlayerShip ship) {
		this.player = ship;
	}
	*/
	@Override
	public void show() {
		shipBullets = new ArrayList<Bullet>();
		enemyBullets = new ArrayList<Bullet>();
		poolBullet = BulletPool.getInstance();
		currentTime = 0;
		String pathTextFont = DefaultResources.getPathByAlias(TEXT_FONT);
		FreeTypeFontGenerator secondFont = infoPack.manager.get(pathTextFont);
		FreeTypeFontParameter parameterSecond = new FreeTypeFontParameter();
		parameterSecond.size = 15;
		parameterSecond.borderWidth = 1;
		parameterSecond.borderColor = Color.GRAY;
		textFont = secondFont.generateFont(parameterSecond);
		processor = new InputProcessorFirstPhase(player);
	}
	
	@Override
	public void dispose() {
		shipBullets = null;
		enemyBullets = null;
		poolBullet = null;
	}
	
	@Override
	public void finishGame() {
		changeable.changeScreen(availablePhases[0]);
	}
	
	public void update(float delta) {
		player.update(delta);
		Iterator<Bullet> alyBullets = shipBullets.iterator();
		while(alyBullets.hasNext()) {
			Bullet currBullet = alyBullets.next();
			//if (currentTime >= timeToCheck) {
				if (!validateBullet(currBullet, true)) {
					//shipBullets.remove(currBullet);
					alyBullets.remove();
					poolBullet.checkIn(currBullet);
				} else {
					currBullet.update(delta);
				}
				//toReset = true;
			//} else {
			//	currBullet.update(delta);
			//}
		}
		Iterator<Bullet> eneBullets = enemyBullets.iterator();
		while(eneBullets.hasNext()) {
			Bullet currBullet = eneBullets.next();
			//if (toReset) {
				if (!validateBullet(currBullet, false)) {
					eneBullets.remove();
					poolBullet.checkIn(currBullet);
				} else {
					currBullet.update(delta);
				}
			//} else {
			//	currBullet.update(delta);
			//}
		}
		
		currentTime += delta;
		if (spacePressed) {
			if (currentTime >= timeToCheck) {
				shipBullets.addAll(player.generateBullets());
				currentTime = 0;
			}
		}
	}
	
	public void render() {
		//infoLog.info("framered " + delta);  
		
		
		//infoLog.info("Active bullets: " + shipBullets.size());
		/*for (Bullet alyBull : shipBullets) {
			//System.out.println(alyBull.toString());
			if (!validateBullet(alyBull)) {
				poolBullet.checkIn(alyBull);
			}
			alyBull.update(delta);
		}*/
		//shipBullets.addAll(player.generateBullets());
		//player.addNewReinforcement(50, 90);
		
		//boolean toReset = false;
		//List<Bullet> bulletToRemoveShip = new ArrayList<Bullet>();
		//List<Bullet> bulletToRemoveEnemy = new ArrayList<Bullet>();
		
		//infoPack.batch.begin();
		player.draw(infoPack.batch);
		String textToShowReinf = "Reforços: " + player.getNumReinforcements();
		for (Bullet alyBull : shipBullets) {
			alyBull.draw(infoPack.batch);
		}
		for (Bullet eneBull : enemyBullets) {
			eneBull.draw(infoPack.batch);
		}
		textFont.draw(infoPack.batch, "Vida restant: " + player.getLife(), 20, 20);
		textFont.draw(infoPack.batch, textToShowReinf, infoPack.screenWidth - textFont.getBounds(textToShowReinf).width - 20, 20);
		
		//infoPack.batch.end();
		
		
		
		//if (toReset) {
		//	currentTime = 0;
		//}
		/*for (Bullet eneBull : enemyBullets) {
			eneBull.update(delta);
		}*/
		
		
		//infoPack.batch.draw(linearBullet1, 50, 50);
		
		//for ()
		/*int x = 0;
		int y = 0;
		for (TextureRegion txt : anim.getKeyFrames()) {
			infoPack.batch.draw(txt, x, y);
			x += 50;
			y += 50;
		}*/
			
		
	}
	
	private boolean validateBullet(Bullet bullet, boolean playerBullet) {
		if (infoPack.screenWidth + MARGIN_PHASE <= bullet.getX()) {
			return false;
		} else if (infoPack.screenHeight + MARGIN_PHASE <= bullet.getY()) {
			return false;
		} else if (-MARGIN_PHASE >= bullet.getY() 
				|| -MARGIN_PHASE >= bullet.getX()) {
			return false;
		} else {
			//for ()
			int bulletDamage = bullet.getDamage();
			ShapeCollideable collideable = bullet.getCollideable();
			if (!playerBullet) {
				for (ICollisionHealtheable coll : player.getCollisionableHealtheables()) {
					if (coll.getCollideable().collide(collideable)) {
						
						int collDamage = coll.getDamage();
						bullet.substractDamage(collDamage);
						try {
							//System.out.println(coll.getDamage());
							coll.substractLife(bulletDamage);
							
						} catch (NoLifeException e) {
							player.removeCollisionableHealtheable(coll);
						}
						if (bullet.getDamage() < 0)
							return false;
					}
				}
				if (player.getCollideable().collide(collideable)) {
					int playerDamage = player.getLife();
					bullet.substractDamage(playerDamage);
					try {
						player.substractLife(bulletDamage);
					} catch (NoLifeException e) {
						// TODO canviar pantalla
						//toChange = availablePhases[0];
					}
					
				}
			} else {
				for (ICollisionHealtheable coll : enemyCollisionable) {
					if (collideable.collide(coll.getCollideable())) {
						int currDamage = coll.getDamage();
						bullet.substractDamage(currDamage);
						try {
							coll.substractLife(bulletDamage);
						} catch (NoLifeException e) {
							// TODO guanyar partida
							//e.printStackTrace();
						}
					}
				}
			}
			
			if (bullet.getDamage() < 0)
				return false;
			
			Iterator<Bullet> currentBullets;
			if (playerBullet) {
				currentBullets = enemyBullets.iterator();
			} else {
				currentBullets = shipBullets.iterator();
			}
				 
			while(currentBullets.hasNext()) {
				Bullet currBullet = currentBullets.next();
				if (currBullet.getCollideable().collide(collideable)) {
					int currBullDamage = currBullet.getDamage();
					bullet.substractDamage(currBullDamage);
					currBullet.substractDamage(bulletDamage);
					
					if (currBullet.getDamage() < 0) {
						currentBullets.remove();
						poolBullet.checkIn(currBullet);
					}
					if (bullet.getDamage() < 0) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public void setPlayerShip(PlayerShip ship) {
		this.player = ship;
	}

	@Override
	public void addNewBullet(Bullet newBullet, boolean enemyBullet) {
		if (enemyBullet) {
			enemyBullets.add(newBullet);
		} else {
			shipBullets.add(newBullet);
		}
	}
	
	private class InputProcessorFirstPhase extends InputAdapter {
		
		private MovableShip movable;
		
		public InputProcessorFirstPhase(MovableShip movable) {
			this.movable = movable;
		}
		
		@Override
		public boolean keyDown(int keycode) {
			if (keycode == Input.Keys.W) {
				movable.setUpMove(true);
				return true;
			}
			if (keycode == Input.Keys.A) {
				movable.setLeftMove(true);
				return true;
			}
			if (keycode == Input.Keys.S) {
				movable.setDownMove(true);
				return true;
			}
			if (keycode == Input.Keys.D) {
				movable.setRightMove(true);
				return true;
			}
			if (keycode == Input.Keys.SPACE) {
				/*System.out.println("----");
				for (Bullet bull : shipBullets) {
					System.out.println(bull.getX() + " " + bull.getY() + " " + bull.getWidth() + " " + bull.getHeight());
				}*/
				//shipBullets.addAll(player.generateBullets());
				spacePressed = true;
				return true;
			}
			if (keycode == Input.Keys.Q) {
				player.addNewReinforcement(50, 120);
				return true;
			}
			return false;
		}
		
		@Override
		public boolean keyTyped(char character) {
			//return super.keyTyped(character);
			if (character == ' ') {
				//shipBullets.addAll(player.generateBullets());
				return true;
			}
			return false;
		}
		
		@Override
		public boolean keyUp(int keycode) {
			if (keycode == Input.Keys.W) {
				movable.setUpMove(false);
				return true;
			}
			if (keycode == Input.Keys.A) {
				movable.setLeftMove(false);
				return true;
			}
			if (keycode == Input.Keys.S) {
				movable.setDownMove(false);
				return true;
			}
			if (keycode == Input.Keys.D) {
				movable.setRightMove(false);
				return true;
			}
			if (keycode == Input.Keys.SPACE) {
				spacePressed = false;
				return true;
			}
			return false;
		}
		
	}
}
