package com.example.m08.nausjoc.screen;

public interface ScreenChangeable {
	public void changeScreen(String screenId);
	public void exitGame();
}
