package com.example.m08.nausjoc.screen;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Logger;
import com.example.m08.nausjoc.boss.GenericBoss;
import com.example.m08.nausjoc.boss.IBehavior;
import com.example.m08.nausjoc.boss.IBulletRemovable;
import com.example.m08.nausjoc.boss.IMusicChanger;
import com.example.m08.nausjoc.boss1.FirstBossFirstBehavior;
import com.example.m08.nausjoc.load.DefaultResources;
import com.example.m08.nausjoc.model.Bullet;
import com.example.m08.nausjoc.parameters.Constants;
import com.example.m08.nausjoc.shapeutils.CollideRectangle;
import com.example.m08.nausjoc.shapeutils.ShapeCollideable;
import com.example.m08.nausjoc.utils.GameInfoPackage;

public class FirstPhase extends AbstractGameScreen implements IMusicChanger, IBulletRemovable {
	
	private Texture circBullet1;
	private Texture circBullet2;
	private Texture circBullet3;
	private Texture circBullet4;
	
	private float timeToNewReinforcement;
	private float currTimeToNewReinforcement;
	
	private Texture firstBossTexture;
	//private Texture secondBoss;
	//private Texture thirdBoss;
	
	private Music firstMusic;
	private Music secondMusic;
	//private Music thirdMusic;
	
	
	
	//private PlayerShip player;
	private GenericBoss firstBoss;
	
	
	//private String toChange;
	
	
	//private boolean spacePressed;
	//private Animation anim;
	
	public FirstPhase(String screenId, GameInfoPackage infoPack,
			ScreenChangeable changeable, Logger infoLog,
			String[] availablePhases) {
		super(screenId, infoPack, changeable, infoLog/*, inpChange*/, availablePhases);
		timeToNewReinforcement = 1;
		currTimeToNewReinforcement = 0;
		//spacePressed = false;
		//toChange = null;
	}

	@Override
	public void show() {
		super.show();
		String pathTextureFirstBoss = DefaultResources.getPathByAlias(Constants.TEXT_BOSS1);
		//String pathTextureSecondBoss = DefaultResources.getPathByAlias(Constants.TEXT_BOSS2);
		//String pathTextureThirdBoss = DefaultResources.getPathByAlias(Constants.TEXT_BOSS3);
		
		String pathMusicFirst = DefaultResources.getPathByAlias(Constants.MUSIC_FACING);
		String pathMusicSecond = DefaultResources.getPathByAlias(Constants.MUSIC_AMUNRA);
		//String pathMusicThird = DefaultResources.getPathByAlias(Constants.MUSIC_WANDERER);
		
		String pathCircBulletFirs = DefaultResources.getPathByAlias(Constants.CIRC_BULLET1);
		String pathCircBulletSeco = DefaultResources.getPathByAlias(Constants.CIRC_BULLET2);
		String pathCircBulletThir = DefaultResources.getPathByAlias(Constants.CIRC_BULLET3);
		String pathCircBulletFour = DefaultResources.getPathByAlias(Constants.CIRC_BULLET4);
		
		
		
		circBullet1 = infoPack.manager.get(pathCircBulletFirs, Texture.class);
		circBullet2 = infoPack.manager.get(pathCircBulletSeco, Texture.class);
		circBullet3 = infoPack.manager.get(pathCircBulletThir, Texture.class);
		circBullet4 = infoPack.manager.get(pathCircBulletFour, Texture.class);
		
		firstBossTexture = infoPack.manager.get(pathTextureFirstBoss, Texture.class);
		//secondBoss = infoPack.manager.get(pathTextureSecondBoss, Texture.class);
		//assetsLoaded++;
		//thirdBoss = infoPack.manager.get(pathTextureThirdBoss, Texture.class);
		//assetsLoaded++;
		firstMusic = infoPack.manager.get(pathMusicFirst, Music.class);
		secondMusic = infoPack.manager.get(pathMusicSecond, Music.class);
		//assetsLoaded++;
		//thirdMusic = infoPack.manager.get(pathMusicThird, Music.class);
		
		//boss.setX(screenWidth / 2);
		//boss.setY(screenHeight - boss.getHeight());
		
		int x = infoPack.screenWidth / 2;
		int y = infoPack.screenHeight;
		int width = 64;
		int height = 128;
		int health = 10000;
		y -= height;
		ShapeCollideable collideable = new CollideRectangle(x, y, width, height);
		firstBoss = new GenericBoss(firstBossTexture, x, y, width, height, health, collideable);
		IBehavior firstBehavior = new FirstBossFirstBehavior(firstBoss, infoPack.screenWidth, 
				infoPack.screenHeight, this, this, this, this, circBullet1, circBullet2, circBullet3, circBullet4);
		firstBoss.setBehavior(firstBehavior);
		enemyCollisionable.add(firstBoss);
		firstMusic.play();
	}
	
	

	@Override
	public void render(float delta) {
		currTimeToNewReinforcement += delta;
		if (currTimeToNewReinforcement >= timeToNewReinforcement) {
			player.addNewReinforcement();
			currTimeToNewReinforcement = 0;
		}
		firstBoss.update(delta);
		super.update(delta);
		
		
		infoPack.batch.begin();
		firstBoss.draw(infoPack.batch);
		super.render();
		String textLifeBoss = "Vida restant boss: " + firstBoss.getLife();
		textFont.draw(infoPack.batch, textLifeBoss, 20, infoPack.screenHeight - textFont.getBounds(textLifeBoss).height - 20);
		
		infoPack.batch.end();
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		super.dispose();
		circBullet1.dispose();
		circBullet2.dispose();
		circBullet3.dispose();
		circBullet4.dispose();;
		
		firstBossTexture.dispose();
		
		firstMusic.dispose();
		secondMusic.dispose();
		
		firstBoss.dispose();
		
	}

	@Override
	public InputProcessor getInputProcessor() {
		return processor;
	}

	@Override
	public void nextMusic() {
		if (firstMusic.isPlaying()) {
			firstMusic.stop();
			secondMusic.play();
		} else if (secondMusic.isPlaying()) {
			secondMusic.stop();
			firstMusic.play();
		}
	}

	@Override
	public void removeBullet(Bullet bullet, boolean enemyBullet) {
		if (enemyBullet)
			enemyBullets.remove(bullet);
		else
			shipBullets.remove(bullet);
	}

	/*@Override
	public int getProgress() {
		return assetsLoaded;
	}

	@Override
	public int getMaxiumProgress() {
		return assetsToLoad;
	}

	@Override
	public void onLoad() {
		if (!phaseBuilded) {
			infoLog.info("Construïnt fase.");
			buildScreen();
			changeable.changeScreen(screenId);
		}
	}

	@Override
	public boolean isLoaded() {
		return assetsLoaded >= assetsToLoad;
	}

	@Override
	public void loadAssets() {
		
		
		
	}

	@Override
	public void buildScreen() {
		
		//playerAtlas.
	}*/
	
	

	

}
