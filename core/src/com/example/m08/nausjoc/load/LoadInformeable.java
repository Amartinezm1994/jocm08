package com.example.m08.nausjoc.load;

public interface LoadInformeable {
	public int getProgress();
	public int getMaxiumProgress();
	public void onLoad();
}
