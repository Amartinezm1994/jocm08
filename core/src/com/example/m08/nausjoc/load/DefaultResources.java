package com.example.m08.nausjoc.load;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Hashtable;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;


public class DefaultResources {
	
	
	public static Hashtable<String, Class<?>> DEFINED_TYPE_BY_STRING;
	private static Hashtable<String, String> RESOURCE_ALIASES;
	//public static int LOADED_ASSETS
	
	static {
		DEFINED_TYPE_BY_STRING = new Hashtable<String, Class<?>>();
		RESOURCE_ALIASES = new Hashtable<String, String>();
		
		DEFINED_TYPE_BY_STRING.put("texture", Texture.class);
		DEFINED_TYPE_BY_STRING.put("atlas", TextureAtlas.class);
		DEFINED_TYPE_BY_STRING.put("json", Skin.class);
		DEFINED_TYPE_BY_STRING.put("music", Music.class);
		DEFINED_TYPE_BY_STRING.put("freefont", FreeTypeFontGenerator.class);
	}
	
	public static void loadAllResources(AssetManager manager, FileHandle file) throws IOException {
		//FileHandle file = Gdx.files.internal(RESOURCE_INFO_PATH);
		BufferedReader reader = new BufferedReader(file.reader());
		String line;
		String separator = reader.readLine();
		while ((line = reader.readLine()) != null) {
			String[] parts = line.split(separator);
			
			RESOURCE_ALIASES.put(parts[0], parts[1]);
			/*if (parts[2].equals("freefont")) {
				FreeTypeFontLoaderParameter params = new FreeTypeFontLoaderParameter();
				params.fontFileName = parts[1];
				params.fontParameters.size = 10;
				manager.load(parts[1], BitmapFont.class, params);
			} else {*/
			manager.load(parts[1], DEFINED_TYPE_BY_STRING.get(parts[2]));
			//}
			
		}
		//Long longg;
		//longg.
		reader.close();
	}
	
	/*public static void loadResources(AssetManager manager) {
		for (Entry<String, Class<?>> ent : ALIASES_TO_LOAD.entrySet()) {
			System.out.println(RESOURCE_ALIASES.get(ent.getKey()));
			manager.load(RESOURCE_ALIASES.get(ent.getKey()), ent.getClass());
		}
	}*/
	
	public static String getPathByAlias(String alias) {
		return RESOURCE_ALIASES.get(alias);
	}
	
}
