package com.example.m08.nausjoc.load;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Logger;
import com.example.m08.nausjoc.utils.GameInfoPackage;

public class LoadingScreen implements Screen  {
	
	private GameInfoPackage infoPackage;
	
	private BitmapFont wordLoading;
	private ShapeRenderer shapeRender;
	private LoadInformeable informeable;
	private Logger infoLog;
	private int lastProgress;

	
	public LoadingScreen(GameInfoPackage infoPackage, LoadInformeable informeable, Logger infoLog) {
		
		this.infoPackage = infoPackage;
		this.informeable = informeable;
		this.infoLog = infoLog;
		
	}

	@Override
	public void show() {
		wordLoading = new BitmapFont();
		shapeRender = new ShapeRenderer();
		lastProgress = 0;
	}

	@Override
	public void render(float delta) {
		float widthProgressBar = (infoPackage.screenWidth / informeable.getMaxiumProgress()) * informeable.getProgress();
		infoPackage.batch.begin();
		wordLoading.draw(infoPackage.batch, "Cargando... " , 15, 30);
		infoPackage.batch.end();
		
		shapeRender.begin(ShapeType.Filled);
		shapeRender.setColor(Color.ORANGE);
		shapeRender.rect(0, 5, widthProgressBar, 10);
		shapeRender.end();
		if (lastProgress < informeable.getProgress()) {
			lastProgress = informeable.getProgress();
			infoLog.info("Carregant " +  informeable.getProgress() + " de " + informeable.getMaxiumProgress());
		}
		if (informeable.getProgress() >= informeable.getMaxiumProgress()) {
			informeable.onLoad();
		}
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void dispose() {
		wordLoading.dispose();
		shapeRender.dispose();
	}

}
