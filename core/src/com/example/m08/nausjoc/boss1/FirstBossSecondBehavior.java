package com.example.m08.nausjoc.boss1;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.util.Random;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.example.m08.nausjoc.boss.GenericBoss;
import com.example.m08.nausjoc.boss.IBehavior;
import com.example.m08.nausjoc.boss.IBulletRemovable;
import com.example.m08.nausjoc.boss.IBulleteable;
import com.example.m08.nausjoc.boss.IFinishGame;
import com.example.m08.nausjoc.boss.IMusicChanger;
import com.example.m08.nausjoc.model.Bullet;
import com.example.m08.nausjoc.model.BulletFactory;
import com.example.m08.nausjoc.model.BulletModel;
import com.example.m08.nausjoc.model.PlayerShipReinforcementsPathFactory;
import com.example.m08.nausjoc.model.SpritePath;
import com.example.m08.nausjoc.shapeutils.ShapeType;

public class FirstBossSecondBehavior extends AbstractFirstBossBehavior {
	
	private static final float RATIO_BULLET_FIRST = 0.2f;
	private static final float RATIO_BULLET_SECOND = 0.013f;
	private static final float RATIO_BULLET_THIRD = 1.9f;
	private static final float RATIO_BULLET_FOUR = 0.01f;
	private static final float RATIO_BULLET_FIVE = 1.4f;
	
	private BulletModel firstModel;
	private BulletModel secondModel;
	private BulletModel thirdModel;
	private BulletModel fourthModel;
	private BulletModel fifthModel;
	
	private Random randomProvider;
	
	private float needToTeleport;
	private float currentToTeleport;
	
	private float needToShootOne;
	private float currentToShootOne;
	
	private float needToShootTwo;
	private float currentToShootTwo;
	
	private float needToShootThree;
	private float currentToShootThree;
	private float needToStopShootThree;
	private float currentToStopShootThree;
	private boolean shootingThree;
	
	private float needToShootFour;
	private float currentToShootFour;
	
	private float needToShootFive;
	private float currentToShootFive;
	
	private float centerX;
	private float centerY;
	
	private SpritePath path;
	private float velocity;
	private float distanceFromCenter;
	
	private boolean pendentToTeleport;
	private float timeNeedToPendentToTeleport;
	private float currentToPendentToTeleport;
	private Vector2 pendentPosition;
	
	private Bullet cleanerBullet;
	private Bullet referenceThreeBulletShoot;
	
	private boolean alwaysThird;
	private boolean destructionMode;
	
	public FirstBossSecondBehavior(GenericBoss boss, int screenWidth, 
			int screenHeight, IBulleteable bulleteable, IMusicChanger changerMusic, 
			IFinishGame gameFinisher, IBulletRemovable removable, Texture bulletTextureFirst,
			Texture bulletTextureSecond, Texture bulletTextureThird, Texture bulletTextureFourth) {
		super(boss, screenWidth, screenHeight, bulleteable, changerMusic, gameFinisher, removable,
				bulletTextureFirst, bulletTextureSecond, bulletTextureThird, bulletTextureFourth);
		
		changerMusic.nextMusic();
		firstModel = new BulletModel(circBullet1, 300, 50, 50, ShapeType.CIRCLE);
		secondModel = new BulletModel(circBullet2, 20, 25, 25, ShapeType.CIRCLE);
		thirdModel = new BulletModel(circBullet3, 80, 25, 25, ShapeType.CIRCLE);
		fourthModel = new BulletModel(circBullet4, 10000, (int)(boss.getWidth() * 2), 
				(int)boss.getHeight(), ShapeType.CIRCLE);
		fifthModel = new BulletModel(circBullet4, 2000, 150, 150, ShapeType.CIRCLE);
		
		maxLife = 8000;
		boss.setLife(maxLife);
		randomProvider = new Random();
		
		distanceFromCenter = 50f;
		path = PlayerShipReinforcementsPathFactory.getRotationalPath(distanceFromCenter, 200);
		velocity = 100f;
		
		needToTeleport = 3.5f;
		currentToTeleport = 0f;
		
		needToShootOne = 2f;
		currentToShootOne = 0;
		
		needToShootTwo = 0.1f;
		currentToShootTwo = 0;
		
		needToShootThree = 20f;
		currentToShootThree = 0;
		
		needToStopShootThree = 5f;
		currentToStopShootThree = 0;
		
		shootingThree = false;
		
		needToShootFour = 2f;
		currentToShootFour = 0;
		
		setNewPosition(getNewPosition());
		pendentToTeleport = false;
		timeNeedToPendentToTeleport = 1f;
		currentToPendentToTeleport = 0;
		
		referenceThreeBulletShoot = null;
		
		needToShootFive = 14f;
		currentToShootFive = 0;
		alwaysThird = false;
		destructionMode = false;
	}
	public FirstBossSecondBehavior(AbstractFirstBossBehavior behaviorModel) {
		this(behaviorModel.getBoss(), behaviorModel.getScreenWidth(), behaviorModel.getScreenHeight(), 
				behaviorModel.getBulleteable(), behaviorModel.getChangerMusic(),
				behaviorModel.getGameFinisher(), behaviorModel.getRemovableBullet(),
				behaviorModel.getCircBullet1(), behaviorModel.getCircBullet2(), 
				behaviorModel.getCircBullet3(), behaviorModel.getCircBullet4());
	}
	
	public void setNewPosition(Vector2 position) {
		centerX = position.x;
		centerY = position.y;
	}
	public Vector2 getNewPosition() {
		Vector2 vec = new Vector2();
		vec.x = randomProvider.nextFloat() * ((screenWidth - boss.getWidth() - distanceFromCenter) - distanceFromCenter) + distanceFromCenter;
		vec.y = randomProvider.nextFloat() * ((screenHeight - boss.getHeight() - distanceFromCenter) - (screenHeight / 2 + distanceFromCenter)) + (screenHeight / 2 + distanceFromCenter);
		return vec;
	}

	@Override
	public void update(float delta) {
		
		Vector2 nextPoint = path.getNextPoint(delta * velocity);
		boss.setPosition(centerX + nextPoint.x , centerY + nextPoint.y);
		if (pendentToTeleport) {
			currentToPendentToTeleport += delta;
			if (currentToPendentToTeleport >= timeNeedToPendentToTeleport) {
				pendentToTeleport = false;
				setNewPosition(pendentPosition);
				removableBullet.removeBullet(cleanerBullet, true);
				cleanerBullet = null;
			}
		} else {
			currentToTeleport += delta;
			if (currentToTeleport >= needToTeleport) {
				currentToTeleport = 0;
				currentToPendentToTeleport = 0;
				pendentToTeleport = true;
				pendentPosition = getNewPosition();
				SpritePath pathBullet = PlayerShipReinforcementsPathFactory.getRotationalPath(
						pendentPosition.x, pendentPosition.y, (int)(distanceFromCenter * 1.5), 20);
				Vector2 currPosBull = pathBullet.getCurrentPoint();
				cleanerBullet = factory.getBulletCustomPath(fourthModel.getTexture(), (int)(currPosBull.x), 
						(int)(currPosBull.y), fourthModel.getWidth(), fourthModel.getHeight(), 
						fourthModel.getDamage(), ShapeType.CIRCLE, pathBullet);
				bulleteable.addNewBullet(cleanerBullet, true);
			}
		}
		//if (cleanerBullet != null)
		//	System.out.println(cleanerBullet.getX() + " " + cleanerBullet.getY());
		currentToShootOne += delta;
		if (currentToShootOne >= needToShootOne) {
			currentToShootOne = 0;
			bulleteable.addNewBullet(
					factory.getLinearBullet(firstModel, (int)(boss.getX() + boss.getWidth() / 2), 
							(int)boss.getY(), BulletFactory.DOWN, 200, 10), true);
			bulleteable.addNewBullet(
					factory.getLinearBullet(firstModel, (int)(boss.getX()), 
							(int)(boss.getY() + boss.getHeight() / 2), BulletFactory.LEFT, 200, 10), true);
			bulleteable.addNewBullet(
					factory.getLinearBullet(firstModel, (int)(boss.getX() + boss.getWidth()), 
							(int)(boss.getY() + boss.getHeight() / 2), BulletFactory.RIGHT, 200, 10), true);
			bulleteable.addNewBullet(
					factory.getLinearBullet(firstModel, (int)(boss.getX() + boss.getWidth() / 2), 
							(int)(boss.getY() + boss.getHeight()), BulletFactory.UP, 200, 10), true);
			if (destructionMode) {
				bulleteable.addNewBullet(
						factory.getLinearBullet(firstModel, (int)(boss.getX() + boss.getWidth()), 
								(int)boss.getY(), BulletFactory.DOWN_RIGHT, 200, 10), true);
				bulleteable.addNewBullet(
						factory.getLinearBullet(firstModel, (int)(boss.getX()), 
								(int)boss.getY(), BulletFactory.DOWN_LEFT, 200, 10), true);
				bulleteable.addNewBullet(
						factory.getLinearBullet(firstModel, (int)(boss.getX() + boss.getWidth() / 2), 
								(int)boss.getY(), BulletFactory.DOWN, 200, 10), true);
				bulleteable.addNewBullet(
						factory.getLinearBullet(firstModel, (int)(boss.getX() + boss.getWidth() / 1.5f), 
								(int)boss.getY(), BulletFactory.DOWN_RIGHT_CENTER, 200, 10), true);
				bulleteable.addNewBullet(
						factory.getLinearBullet(firstModel, (int)(boss.getX() + boss.getWidth() / 4), 
								(int)boss.getY(), BulletFactory.DOWN_LEFT_CENTER, 200, 10), true);
			}
			/*bulleteable.addNewBullet(
					factory.getLinearBullet(secondModel, (int)(boss.getX() + boss.getWidth()), 
							(int)boss.getY(), BulletFactory.DOWN_RIGHT, 200, 10), true);*/
		}
		currentToShootTwo += delta;
		if (currentToShootTwo >= needToShootTwo) {
			currentToShootTwo = 0;
			int numBullets = 1;
			
			for (int i = 0; i < numBullets; i++) {
				bulleteable.addNewBullet(
						factory.getLinearBullet(secondModel, (int)(boss.getX() + boss.getWidth()), 
								(int)boss.getY(), BulletFactory.DOWN_RIGHT, 200, 10), true);
				bulleteable.addNewBullet(
						factory.getLinearBullet(secondModel, (int)(boss.getX()), 
								(int)boss.getY(), BulletFactory.DOWN_LEFT, 200, 10), true);
				bulleteable.addNewBullet(
						factory.getLinearBullet(secondModel, (int)(boss.getX() + boss.getWidth() / 2), 
								(int)boss.getY(), BulletFactory.DOWN, 200, 10), true);
				bulleteable.addNewBullet(
						factory.getLinearBullet(secondModel, (int)(boss.getX() + boss.getWidth() / 1.5f), 
								(int)boss.getY(), BulletFactory.DOWN_RIGHT_CENTER, 200, 10), true);
				bulleteable.addNewBullet(
						factory.getLinearBullet(secondModel, (int)(boss.getX() + boss.getWidth() / 4), 
								(int)boss.getY(), BulletFactory.DOWN_LEFT_CENTER, 200, 10), true);
				if (destructionMode) {
					bulleteable.addNewBullet(
							factory.getLinearBullet(secondModel, (int)(boss.getX()- secondModel.getWidth()), 
									(int)(boss.getY() + boss.getHeight() / 4), BulletFactory.LEFT, 200, 10), true);
					bulleteable.addNewBullet(
							factory.getLinearBullet(secondModel, (int)(boss.getX()- secondModel.getWidth()), 
									(int)(boss.getY() + boss.getHeight() / 2), BulletFactory.LEFT, 200, 10), true);
					bulleteable.addNewBullet(
							factory.getLinearBullet(secondModel, (int)(boss.getX()- secondModel.getWidth()), 
									(int)(boss.getY() + boss.getHeight() / 1.5), BulletFactory.LEFT, 200, 10), true);
					bulleteable.addNewBullet(
							factory.getLinearBullet(secondModel, (int)(boss.getX() + boss.getWidth()), 
									(int)(boss.getY() + boss.getHeight() / 4), BulletFactory.RIGHT, 200, 10), true);
					bulleteable.addNewBullet(
							factory.getLinearBullet(secondModel, (int)(boss.getX() + boss.getWidth()), 
									(int)(boss.getY() + boss.getHeight() / 2), BulletFactory.RIGHT, 200, 10), true);
					bulleteable.addNewBullet(
							factory.getLinearBullet(secondModel, (int)(boss.getX() + boss.getWidth()), 
									(int)(boss.getY() + boss.getHeight() / 1.5), BulletFactory.RIGHT, 200, 10), true);
				}
			}
		}
		
		if (shootingThree || alwaysThird) {
			currentToStopShootThree += delta;
			if (currentToStopShootThree >= needToStopShootThree) {
				referenceThreeBulletShoot = null;
				shootingThree = false;
				currentToStopShootThree = 0;
			} else {
				if (referenceThreeBulletShoot == null ||
						thirdModel.getHeight() + referenceThreeBulletShoot.getY() < 
						screenHeight - thirdModel.getHeight()) {
					int separationScreen = thirdModel.getWidth();
					int numBulletsScreen = screenWidth / (separationScreen);
					for (int i = 0; i < numBulletsScreen; i++) {
						Bullet bulletToAdd = factory.getLinearBullet(thirdModel, (int)(separationScreen * i), 
								(int)(screenHeight - thirdModel.getHeight()), 
								BulletFactory.DOWN, 200, 10);
						if (i == 0) {
							referenceThreeBulletShoot = bulletToAdd;
							//System.out.println(referenceThreeBulletShoot);
						}
						bulleteable.addNewBullet(bulletToAdd, true);
					}
				}
			}
		} else {
			currentToShootThree += delta;
			if (currentToShootThree >= needToShootThree) {
				currentToShootThree = 0;
				shootingThree = true;
				currentToStopShootThree = 0;
			}
		}
		currentToShootFour += delta;
		if (currentToShootFour >= needToShootFour) {
			currentToShootFour = 0;
		}
		currentToShootFive += delta;
		if (currentToShootFive >= needToShootFive) {
			currentToShootFive = 0;
			bulleteable.addNewBullet(
					factory.getLinearBullet(fifthModel, (int)(boss.getX() + boss.getWidth() / 2), 
							(int)boss.getY(), BulletFactory.DOWN, 600, 2), true);
		}
		
		if (!path.hasNext()) {
			path.resetPath();
		}
		
		//System.out.println((centerX + (centerX - nextPoint.x)) + " " + (centerY + (centerY - nextPoint.y)));
	}

	@Override
	public IBehavior getNextBehavior() {
		return null;
	}
	
	//private boolean ninthyAct = false;
	private boolean eightyAct = false;
	//private boolean seventhyAct = false;
	private boolean sixthyAct = false;
	//private boolean fifthyAct = false;
	private boolean fourthyAct = false;
	//private boolean thirthyAct = false;
	private boolean twenthyAct = false;
	//private boolean tenAct = false;

	@Override
	public void vetoableChange(PropertyChangeEvent evt)
			throws PropertyVetoException {
		int percLife = (Integer)evt.getNewValue() * 100 / maxLife;
		//System.out.println(percLife);
		/*if (!ninthyAct && percLife < 90) {
			needToShootOne -= RATIO_BULLET_FIRST;
			needToShootTwo -= RATIO_BULLET_SECOND;
			//needToShootThree -= RATIO_BULLET_THIRD;
			needToShootFive -= RATIO_BULLET_FIVE;
			ninthyAct = true;
			
		}*/
		if (!eightyAct && percLife < 80) {
			needToShootOne -= RATIO_BULLET_FIRST;
			needToShootTwo -= RATIO_BULLET_SECOND;
			//needToShootThree -= RATIO_BULLET_THIRD;
			needToShootFive -= RATIO_BULLET_FIVE;
			eightyAct = true;
		}
		/*if (!seventhyAct && percLife < 70) {
			needToShootOne -= RATIO_BULLET_FIRST;
			needToShootTwo -= RATIO_BULLET_SECOND;
			//needToShootThree -= RATIO_BULLET_THIRD;
			needToShootFive -= RATIO_BULLET_FIVE;
			seventhyAct = true;
		}*/
		if (!sixthyAct && percLife < 60) {
			needToShootOne -= RATIO_BULLET_FIRST * 2;
			needToShootTwo -= RATIO_BULLET_SECOND * 2;
			//needToShootThree -= RATIO_BULLET_THIRD;
			needToShootFive -= RATIO_BULLET_FIVE * 2;
			sixthyAct = true;
		}
		/*if (!fifthyAct && percLife < 50) {
			needToShootOne -= RATIO_BULLET_FIRST;
			needToShootTwo -= RATIO_BULLET_SECOND;
			//needToShootThree -= RATIO_BULLET_THIRD;
			needToShootFive -= RATIO_BULLET_FIVE;
			fifthyAct = true;
		}*/
		if (!fourthyAct && percLife < 40) {
			needToShootOne -= RATIO_BULLET_FIRST * 4;
			needToShootTwo -= RATIO_BULLET_SECOND * 4;
			//needToShootThree -= RATIO_BULLET_THIRD;
			needToShootFive -= RATIO_BULLET_FIVE * 4;
			fourthyAct = true;
			destructionMode = true;
		}
		/*if (!thirthyAct && percLife < 30) {
			needToShootOne -= RATIO_BULLET_FIRST;
			needToShootTwo -= RATIO_BULLET_SECOND;
			//needToShootThree -= RATIO_BULLET_THIRD;
			needToShootFive -= RATIO_BULLET_FIVE;
			
			thirthyAct = true;
		}*/
		if (!twenthyAct && percLife < 20) {
			//needToShootOne -= RATIO_BULLET_FIRST * 4;
			//needToShootTwo -= RATIO_BULLET_SECOND * 4;
			//needToShootThree -= RATIO_BULLET_THIRD;
			//needToShootFive -= RATIO_BULLET_FIVE * 4;
			twenthyAct = true;
			alwaysThird = true;
		}
		/*if (!tenAct && percLife < 10) {
			needToShootOne -= RATIO_BULLET_FIRST;
			needToShootTwo -= RATIO_BULLET_SECOND;
			//needToShootThree -= RATIO_BULLET_THIRD;
			needToShootFive -= RATIO_BULLET_FIVE;
			tenAct = true;
			
		}*/
	}
	
}
