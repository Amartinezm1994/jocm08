package com.example.m08.nausjoc.boss1;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;

import com.badlogic.gdx.graphics.Texture;
import com.example.m08.nausjoc.boss.GenericBoss;
import com.example.m08.nausjoc.boss.IBehavior;
import com.example.m08.nausjoc.boss.IBulletRemovable;
import com.example.m08.nausjoc.boss.IBulleteable;
import com.example.m08.nausjoc.boss.IFinishGame;
import com.example.m08.nausjoc.boss.IMusicChanger;
import com.example.m08.nausjoc.model.BulletFactory;
import com.example.m08.nausjoc.model.BulletModel;
import com.example.m08.nausjoc.shapeutils.ShapeType;

public class FirstBossFirstBehavior extends AbstractFirstBossBehavior {
	
	private static final float RATIO_BULLET_SMALL = 0.02f;
	private static final float RATIO_BULLET_BIG = 0.45f;
	
	private float timeToShootOne;
	private float timeNeedToShootOne;
	
	private float timeToShootTwo;
	private float timeNeedToShootTwo;
	
	private BulletModel firstModel;
	private BulletModel secondModel;
	
	private float velocity;
	
	private boolean direction;
	
	
	public FirstBossFirstBehavior(GenericBoss boss, int screenWidth, 
			int screenHeight, IBulleteable bulleteable,IMusicChanger changerMusic, 
			IFinishGame gameFinisher, IBulletRemovable removable, Texture bulletTextureFirst,
			Texture bulletTextureSecond, Texture bulletTextureThird, Texture bulletTextureFourth) {
		super(boss, screenWidth, screenHeight, bulleteable, changerMusic, gameFinisher, removable, 
				bulletTextureFirst, bulletTextureSecond, bulletTextureThird, bulletTextureFourth);
		
		firstModel = new BulletModel(circBullet1, 300, 100, 100, ShapeType.CIRCLE);
		secondModel = new BulletModel(circBullet2, 20, 50, 50, ShapeType.CIRCLE);
		
		direction = false;
		velocity = 800f;
		
		timeToShootOne = 0;
		timeToShootTwo = 0;
		timeNeedToShootOne = 4;
		timeNeedToShootTwo = 0.1f;
		
	}
	
	@Override
	public void dispose() {
		super.dispose();
		firstModel = null;
		secondModel = null;
	}
	
	
	@Override
	public IBehavior getNextBehavior() {
		return new FirstBossSecondBehavior(this);
	}
	
	private boolean eigthPulled = false;
	private boolean sixthyPulled = false;
	private boolean fourthyPulled = false;
	
	@Override
	public void vetoableChange(PropertyChangeEvent evt)
			throws PropertyVetoException {
		int newLife = (Integer)evt.getNewValue();
		if (newLife * 100 / maxLife < 80 && !eigthPulled) {
			timeNeedToShootTwo -= RATIO_BULLET_SMALL;
			timeNeedToShootOne -= RATIO_BULLET_BIG;
			eigthPulled = true;
		}
		if (newLife * 100 / maxLife < 60 && !sixthyPulled) {
			timeNeedToShootTwo -= RATIO_BULLET_SMALL * 2;
			timeNeedToShootOne -= RATIO_BULLET_BIG * 2;
			sixthyPulled = true;
		}
		if (newLife * 100 / maxLife < 40 && !fourthyPulled) {
			timeNeedToShootTwo -= RATIO_BULLET_SMALL * 5;
			timeNeedToShootOne -= RATIO_BULLET_BIG * 5;
			fourthyPulled = true;
		}
		if (newLife * 100 / maxLife < 10) {
			throw new PropertyVetoException("", evt);
		}
	}

	@Override
	public void update(float delta) {
		timeToShootOne += delta;
		timeToShootTwo += delta;
		
		if (timeToShootOne >= timeNeedToShootOne) {
			bulleteable.addNewBullet(
					factory.getLinearBullet(firstModel, (int)(boss.getX() + boss.getWidth() / 2 - firstModel.getWidth() / 2), 
							(int)boss.getY(), BulletFactory.DOWN, 200, 10), true);
			bulleteable.addNewBullet(
					factory.getLinearBullet(firstModel, (int)(boss.getX() + boss.getWidth() / 2 - firstModel.getWidth() / 2), 
							(int)boss.getY(), BulletFactory.DOWN_LEFT, 200, 10), true);
			bulleteable.addNewBullet(
					factory.getLinearBullet(firstModel, (int)(boss.getX() + boss.getWidth() / 2 - firstModel.getWidth() / 2), 
							(int)boss.getY(), BulletFactory.DOWN_RIGHT, 200, 10), true);
			timeToShootOne = 0;
			//timeNeedToShootOne -= RATIO_BULLET_BIG * delta;
		}
		if (timeToShootTwo >= timeNeedToShootTwo) {
			//int numNewBullets
			/*bulleteable.addNewBullet(
					factory.getLinearBullet(secondModel, (int)(boss.getX() + boss.getWidth() / 2 - secondModel.getWidth() / 2), 
							(int)boss.getY(), BulletFactory.DOWN, 200, 10), true);
			bulleteable.addNewBullet(
					factory.getLinearBullet(secondModel, (int)(boss.getX() + boss.getWidth() / 2 - secondModel.getWidth() / 2), 
							(int)boss.getY(), BulletFactory.DOWN, 200, 10), true);
			bulleteable.addNewBullet(
					factory.getLinearBullet(secondModel, (int)(boss.getX() + boss.getWidth() / 2 - secondModel.getWidth() / 2), 
							(int)boss.getY(), BulletFactory.DOWN, 200, 10), true);*/
			int numBullets = 1;
			
			for (int i = 0; i < numBullets; i++) {
				bulleteable.addNewBullet(
						factory.getLinearBullet(secondModel, (int)(boss.getX() + boss.getWidth() / 2), 
								(int)boss.getY(), BulletFactory.DOWN, 200, 10), true);
			}
			timeToShootTwo = 0;
		}
		if (boss.getX() <= 0) {
			direction = true;
		} else if (boss.getX() + boss.getWidth() >= screenWidth) {
			direction = false;
		}
		if (direction) {
			boss.move(velocity * delta, 0);
		} else {
			boss.move(-velocity * delta, 0);
		}
	}

}
