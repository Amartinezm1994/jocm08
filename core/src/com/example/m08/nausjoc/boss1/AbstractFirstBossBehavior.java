package com.example.m08.nausjoc.boss1;

import com.badlogic.gdx.graphics.Texture;
import com.example.m08.nausjoc.boss.GenericBoss;
import com.example.m08.nausjoc.boss.IBehavior;
import com.example.m08.nausjoc.boss.IBulletRemovable;
import com.example.m08.nausjoc.boss.IBulleteable;
import com.example.m08.nausjoc.boss.IFinishGame;
import com.example.m08.nausjoc.boss.IMusicChanger;
import com.example.m08.nausjoc.model.BulletFactory;

public abstract class AbstractFirstBossBehavior implements IBehavior {
	protected int screenWidth;
	protected int screenHeight;
	protected GenericBoss boss;
	protected IBulleteable bulleteable;
	
	protected int maxLife;
	
	protected BulletFactory factory;
	
	protected Texture circBullet1;
	protected Texture circBullet2;
	protected Texture circBullet3;
	protected Texture circBullet4;
	
	protected IMusicChanger changerMusic;
	protected IFinishGame gameFinisher;
	protected IBulletRemovable removableBullet;
	
	public AbstractFirstBossBehavior(GenericBoss boss, int screenWidth, 
			int screenHeight, IBulleteable bulleteable, IMusicChanger changerMusic, 
			IFinishGame gameFinisher, IBulletRemovable removableBullet, Texture bulletTextureFirst,
			Texture bulletTextureSecond, Texture bulletTextureThird, Texture bulletTextureFourth) {
		this.boss = boss;
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
		this.bulleteable = bulleteable;
		this.changerMusic = changerMusic;
		this.removableBullet = removableBullet;
		this.gameFinisher = gameFinisher;
		this.circBullet1 = bulletTextureFirst;
		this.circBullet2 = bulletTextureSecond;
		this.circBullet3 = bulletTextureThird;
		this.circBullet4 = bulletTextureFourth;
		//boss.getCollideable().setPosition(boss.getX(), boss.getY());

		
		maxLife = boss.getLife();
		factory = BulletFactory.getInstance();
		
		
	}
	
	@Override
	public void dispose() {
		boss = null;
		bulleteable = null;
		factory = null;
		circBullet1 = null;
		circBullet2 = null;
	}

	public int getScreenWidth() {
		return screenWidth;
	}

	public void setScreenWidth(int screenWidth) {
		this.screenWidth = screenWidth;
	}

	public int getScreenHeight() {
		return screenHeight;
	}

	public void setScreenHeight(int screenHeight) {
		this.screenHeight = screenHeight;
	}

	public GenericBoss getBoss() {
		return boss;
	}

	public void setBoss(GenericBoss boss) {
		this.boss = boss;
	}

	public IBulleteable getBulleteable() {
		return bulleteable;
	}

	public void setBulleteable(IBulleteable bulleteable) {
		this.bulleteable = bulleteable;
	}

	public int getMaxLife() {
		return maxLife;
	}

	public void setMaxLife(int maxLife) {
		this.maxLife = maxLife;
	}

	public BulletFactory getFactory() {
		return factory;
	}

	public void setFactory(BulletFactory factory) {
		this.factory = factory;
	}

	public Texture getCircBullet1() {
		return circBullet1;
	}

	public void setCircBullet1(Texture circBullet1) {
		this.circBullet1 = circBullet1;
	}

	public Texture getCircBullet2() {
		return circBullet2;
	}

	public void setCircBullet2(Texture circBullet2) {
		this.circBullet2 = circBullet2;
	}

	public Texture getCircBullet3() {
		return circBullet3;
	}

	public void setCircBullet3(Texture circBullet3) {
		this.circBullet3 = circBullet3;
	}

	public Texture getCircBullet4() {
		return circBullet4;
	}

	public void setCircBullet4(Texture circBullet4) {
		this.circBullet4 = circBullet4;
	}

	public IMusicChanger getChangerMusic() {
		return changerMusic;
	}

	public void setChangerMusic(IMusicChanger changerMusic) {
		this.changerMusic = changerMusic;
	}

	public IFinishGame getGameFinisher() {
		return gameFinisher;
	}

	public void setGameFinisher(IFinishGame gameFinisher) {
		this.gameFinisher = gameFinisher;
	}

	public IBulletRemovable getRemovableBullet() {
		return removableBullet;
	}

	public void setRemovableBullet(IBulletRemovable removableBullet) {
		this.removableBullet = removableBullet;
	}

}
