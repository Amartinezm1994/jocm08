package com.example.m08.nausjoc.boss;

import com.example.m08.nausjoc.model.Bullet;

public interface IBulleteable {
	public void addNewBullet(Bullet newBullet, boolean enemyBullet);
}
