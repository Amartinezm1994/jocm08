package com.example.m08.nausjoc.boss;

import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.example.m08.nausjoc.model.ICollisionHealtheable;
import com.example.m08.nausjoc.shapeutils.ShapeCollideable;

public class GenericBoss extends Sprite implements ICollisionHealtheable {
	
	private VetoableChangeSupport support;
	
	private int life;
	private ShapeCollideable collideable;
	private IBehavior behavior;
	
	
	public GenericBoss(Texture text, int x, int y, int width, int height,
			int life, ShapeCollideable collideable) {
		super(text, x, y, width, height);
		this.life = life;
		this.collideable = collideable;
		setX(x);
		setY(y);
		support = new VetoableChangeSupport(this);
	}
	
	@Override
	public void draw(Batch batch) {
		batch.draw(getTexture(), getX(), getY(), getWidth(), getHeight());
	}
	
	public void update(float delta) {
		behavior.update(delta);
	}
	
	public void dispose() {
		behavior.dispose();
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		//PropertyChangeEvent event = new PropertyChangeEvent(source, propertyName, oldValue, newValue)
		try {
			support.fireVetoableChange("life", this.life, life);
			this.life = life;
		} catch (PropertyVetoException e) {
			setBehavior(behavior.getNextBehavior());
		}
		
	}
	
	@Override
	public void substractLife(int substraction) {
		setLife(life - substraction);
	}

	public IBehavior getBehavior() {
		return behavior;
	}

	public void setBehavior(IBehavior behavior) {
		support.removeVetoableChangeListener(this.behavior);
		this.behavior = behavior;
		support.addVetoableChangeListener(behavior);
	}
	
	public void move(float x, float y) {
		setX(getX() + x);
		setY(getY() + y);
		collideable.setPosition(getX() + x, getY() + y);
	}
	@Override
	public void setPosition(float x, float y) {
		super.setPosition(x, y);
		collideable.setPosition(x, y);
	}

	@Override
	public ShapeCollideable getCollideable() {
		return collideable;
	}

	@Override
	public int getDamage() {
		return life;
	}
}
