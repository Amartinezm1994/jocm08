package com.example.m08.nausjoc.boss;

import java.beans.VetoableChangeListener;

public interface IBehavior extends VetoableChangeListener {
	public void update(float delta);
	public IBehavior getNextBehavior();
	public void dispose();
}
