package com.example.m08.nausjoc.boss;

import com.example.m08.nausjoc.model.Bullet;

public interface IBulletRemovable {
	public void removeBullet(Bullet bullet, boolean enemyBullet);
}
