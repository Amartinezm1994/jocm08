package com.example.m08.nausjoc.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.example.m08.nausjoc.shapeutils.CollideCircle;
import com.example.m08.nausjoc.shapeutils.CollideRectangle;
import com.example.m08.nausjoc.shapeutils.ShapeCollideable;
import com.example.m08.nausjoc.shapeutils.ShapeType;

public class BulletFactory {
	
	public static final int UP = 1;
	public static final int DOWN = 2;
	public static final int LEFT = 3;
	public static final int RIGHT = 4;
	
	public static final int UP_LEFT = 5;
	public static final int UP_RIGHT = 6;
	public static final int DOWN_LEFT = 7;
	public static final int DOWN_RIGHT = 8;
	
	public static final int DOWN_LEFT_CENTER = 9;
	public static final int DOWN_RIGHT_CENTER = 10;
	
	private static final int DEFAULT_DISTANCE = 1;
	
	private static final float CORNER_DISTANCE = (float)Math.sqrt(DEFAULT_DISTANCE/2f);
	
	private static final float CORNER_HIGH_DISTANCE = (float)Math.sqrt(DEFAULT_DISTANCE);
	private static final float CORNER_LESS_DISTANCE = (float)DEFAULT_DISTANCE * 1.5f;
	
	//private static final float CORNER_LOW_DISTANCE = (float)Math.sqrt(DEFAULT_DISTANCE/1.5f);
	
	private static final Vector2 UPPING_VECTOR = new Vector2(0, DEFAULT_DISTANCE);
	private static final Vector2 DOWNING_VECTOR = new Vector2(0, -DEFAULT_DISTANCE);
	private static final Vector2 LEFTING_VECTOR = new Vector2(-DEFAULT_DISTANCE, 0);
	private static final Vector2 RIGHTING_VECTOR = new Vector2(DEFAULT_DISTANCE, 0);
	
	private static final Vector2 UP_LEFT_CORNER_VECTOR = new Vector2(-CORNER_DISTANCE, CORNER_DISTANCE);
	private static final Vector2 UP_RIGHT_CORNER_VECTOR = new Vector2(CORNER_DISTANCE, CORNER_DISTANCE);
	private static final Vector2 DOWN_LEFT_CORNER_VECTOR = new Vector2(-CORNER_DISTANCE, -CORNER_DISTANCE);
	private static final Vector2 DOWN_RIGHT_CORNER_VECTOR = new Vector2(CORNER_DISTANCE, -CORNER_DISTANCE);
	
	private static final Vector2 DOWN_LEFT_CENTER_CORNER_VECTOR = new Vector2(-CORNER_HIGH_DISTANCE, -CORNER_LESS_DISTANCE);
	private static final Vector2 DOWN_RIGHT_CENTER_CORNER_VECTOR = new Vector2(CORNER_HIGH_DISTANCE, -CORNER_LESS_DISTANCE);
	
	private BulletPool pool;
	
	private static BulletFactory INSTANCE = null;
	
	private BulletFactory() {
		pool = BulletPool.getInstance();
	}
	
	public static BulletFactory getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new BulletFactory();
		}
		return INSTANCE;
	}
	
	/*public static Bullet getLinearBullet(BulletModel model, int x,
			int y, int direction, int numPoints, int distanceBetween) {
		return getLinearBullet(model.getTexture(), x, y, model.getWidth(), 
				model.getHeight(), direction, numPoints, distanceBetween);
	}*/
	
	public Bullet getLinearBullet(BulletModel model, int x, int y,
			int direction, int numPoints, int distanceBetween) {
		return getLinearBullet(model.getTexture(), x, y, model.getWidth(), 
				model.getHeight(), model.getDamage(), direction, numPoints, 
				distanceBetween, model.getTypeBullet());
	}
	
	public Bullet getLinearBullet(Texture text, 
			int x, int y, int width, int height, int damage,
			int direction, int numPoints, int distanceBetween,
			ShapeType typeBullet) {
		
		Vector2 directionalVector = getVector2(direction);
		Array<Vector2> path = new Array<Vector2>();
		for (int i = 0; i < numPoints; i++) {
			
			Vector2 actPath = new Vector2(x + directionalVector.x * distanceBetween * i, 
					y + directionalVector.y * distanceBetween * i);
			path.add(actPath);
		}
		SpritePath spritePath = new SpritePath(path);
		
		return getBulletCustomPath(text, x, y, width, height, 
				damage, typeBullet, spritePath);
	}
	
	public Bullet getBulletCustomPath(Texture text, 
			int x, int y, int width, int height, int damage,
			ShapeType typeBullet, SpritePath path) {
		ShapeCollideable collideable = null;
		switch(typeBullet) {
		case CIRCLE:
			collideable = new CollideCircle(x, y, width);
			break;
		case RECTANGLE:
			collideable = new CollideRectangle(x, y, width, height);
		}
		Bullet bull = pool.checkOut(text, x, y, width, height, damage, collideable);
		bull.setPath(path);
		return bull;
	}
	
	/*public static Bullet getLinearBullet(Texture text, 
			int x, int y, int width, int height, int direction, 
			int numPoints, int distanceBetween) {
		Bullet bull = new Bullet(text, x, y, width, height);
		Vector2 directionalVector = getVector2(direction);
		Array<Vector2> path = new Array<Vector2>();
		for (int i = 0; i < numPoints; i++) {
			Vector2 actPath = new Vector2(x + directionalVector.x * distanceBetween, 
					y + directionalVector.y * distanceBetween);
			path.add(actPath);
		}
		SpritePath spritePath = new SpritePath(path);
		bull.setPath(spritePath);
		return bull;
	}*/
	private static Vector2 getVector2(int direction) {
		if (direction == UP) {
			return UPPING_VECTOR;
		} else if (direction == DOWN) {
			return DOWNING_VECTOR;
		} else if (direction == LEFT) {
			return LEFTING_VECTOR;
		} else if (direction == RIGHT) {
			return RIGHTING_VECTOR;
		} else if (direction == UP_LEFT) {
			return UP_LEFT_CORNER_VECTOR;
		} else if (direction == UP_RIGHT) {
			return UP_RIGHT_CORNER_VECTOR;
		} else if (direction == DOWN_LEFT) {
			return DOWN_LEFT_CORNER_VECTOR;
		} else if (direction == DOWN_RIGHT) {
			return DOWN_RIGHT_CORNER_VECTOR;
		} else if (direction == DOWN_LEFT_CENTER) {
			return DOWN_LEFT_CENTER_CORNER_VECTOR;
		} else if (direction == DOWN_RIGHT_CENTER) {
			return DOWN_RIGHT_CENTER_CORNER_VECTOR;
		}
		throw new IllegalArgumentException("Direction must be a someone of this class constant.");
	}
}
