package com.example.m08.nausjoc.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.example.m08.nausjoc.shapeutils.ShapeCollideable;

public class Bullet extends Sprite {
	
	//https://github.com/libgdx/libgdx/blob/master/gdx/src/com/badlogic/gdx/graphics/g2d/Sprite.java
	
	private SpritePath path;
	private int damage;
	private ShapeCollideable collideable;
	
	
	public Bullet(Texture text, int x, int y, int width, int height, ShapeCollideable collideable) {
		super(text, x, y, width, height);
		this.collideable = collideable;
	}
	public Bullet(BulletModel model, int x, int y, ShapeCollideable collideable) {
		this(model.getTexture(), x, y, model.getWidth(), model.getHeight(), collideable);
		this.damage = model.getDamage();
	}
	public void setPath(SpritePath path) {
		this.path = path;
	}
	public int getDamage() {
		return damage;
	}
	public void setDamage(int damage) {
		this.damage = damage;
	}
	public void substractDamage(int damage) {
		this.damage -= damage;
	}
	public void update(float delta) {
		if (path.hasNext()) {
			Vector2 actualPoint = path.getNextPoint(delta * 20);
			//float xPosMove = actualPoint.x - antPoX;
			//float yPosMove = actualPoint.y - antPoY;
			setPosition(actualPoint.x, actualPoint.y);
			collideable.setPosition(actualPoint.x, actualPoint.y);
			//collideable.setPosition(xPosMove, yPosMove);
		}
		
	}
	
	public void setCollideable(ShapeCollideable collideable) {
		this.collideable = collideable;
	}
	
	public ShapeCollideable getCollideable() {
		return collideable;
	}
	
	@Override
	public void draw(Batch batch) {
		batch.draw(getTexture(), getX(), getY(), getWidth(), getHeight());
		//super.draw(batch);
	}
	
	@Override
	public String toString() {
		return "Bullet en posicio x:" + getX() + " y:" + getY();
	}
}
