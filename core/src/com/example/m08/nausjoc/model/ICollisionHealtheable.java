package com.example.m08.nausjoc.model;

import com.example.m08.nausjoc.shapeutils.ShapeCollideable;

public interface ICollisionHealtheable {
	public ShapeCollideable getCollideable();
	public void substractLife(int life) throws NoLifeException;
	public int getDamage();
}
