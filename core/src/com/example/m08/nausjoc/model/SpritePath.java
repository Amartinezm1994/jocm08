package com.example.m08.nausjoc.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class SpritePath {
	private Array<Vector2> path;
	private int current;
	private float currentDelta;
	
	public SpritePath(Array<Vector2> path, int firstPoint) {
		this.path = path;
		current = firstPoint;
		currentDelta = current;
	}
	public SpritePath(Array<Vector2> path) {
		this(path, 0);
	}
	public void setCurrentPoint(int numPoint) {
		currentDelta = numPoint;
	}
	public int getCurrentIndexPoint(float delta) {
		
		currentDelta += delta;
		//hasNext();
		return (int)currentDelta;
	}
	public SpritePath(Vector2 ... points) {
		Array<Vector2> path = new Array<Vector2>();
		for (int i = 0; i < points.length; i++) {
			path.add(points[i]);
		}
		this.path = path;
		resetPath();
	}
	public Vector2 getCurrentPoint() {
		//hasNext();
		//float thisCurrent = currentDelta;
		if (path.size <= (int)currentDelta) {
			currentDelta = 0;
		}
		//path.get();
		return path.get((int)currentDelta);
	}
	public Vector2 getNextPoint(float delta) {
		if (path.size <= (int)currentDelta) {
			currentDelta = 0;
		}
		Vector2 vec = path.get((int)currentDelta);
		currentDelta += delta;
		//System.out.println(currentDelta);
		return vec;
	}
	public boolean hasNext() {
		//if (!(path.size >= currentDelta)) {
		//	resetPath();
		//}
		return path.size >= currentDelta;
	}
	public void resetPath() {
		current = 0;
		currentDelta = 0;
	}
}
