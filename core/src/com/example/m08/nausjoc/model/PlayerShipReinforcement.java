package com.example.m08.nausjoc.model;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.example.m08.nausjoc.shapeutils.ShapeCollideable;

public class PlayerShipReinforcement extends Sprite implements ICollisionHealtheable {
	
	private int life;
	//private int baseDamage;
	private SpritePath path;
	private Animation animation;
	private float animMoment;
	private BulletModel bulletModel;
	private boolean invertedAnim;
	//private Facto bullPool;
	private BulletFactory factory;
	private ShapeCollideable collideable;
	
	/*public PlayerShipReinforcement(int baseDamage, SpritePath path) {
		//super
		this.baseDamage = baseDamage;
		this.path = path;
		animMoment = 0;
	}*/
	public PlayerShipReinforcement(int life/*, int baseDamage*/, SpritePath path, Animation animation, 
			int width, int height, BulletModel bulletModel, ShapeCollideable collideable) {
		//this(baseDamage, path);
		//TODO
		super(animation.getKeyFrame(0), (int)path.getCurrentPoint().x, (int)path.getCurrentPoint().y, width, height);
		this.bulletModel = bulletModel;
		this.path = path;
		this.animation = animation;
		this.life = life;
		//this.baseDamage = baseDamage;
		this.collideable = collideable;
		//bullPool = BulletPool.getInstance();
		factory = BulletFactory.getInstance();
		animMoment = 0;
	}
	
	public void move(float x, float y) {
		setX(getX() + x); 
		setY(getY() + y);
		collideable.setPosition(getX() + x, getY() + y);
	}
	
	public void setBulletModel(BulletModel bulletModel) {
		this.bulletModel = bulletModel;
	}
	
	public Bullet generateBullet() {
		//Bullet bull = bullPool.checkOut(bulletModel, (int)(getX() + getWidth() / 2), (int)(getY() + getHeight()));
		return factory.getLinearBullet(bulletModel, (int)(getX() + getWidth() / 2), 
				(int)(getY() + getHeight()), BulletFactory.UP, 200, 5);
		//return 	null;
	}
	
	public void update(float delta, float xCurr, float yCurr) {
		if (animation.isAnimationFinished(animMoment)) {
			invertedAnim = true;
		} else if (animMoment < 0) {
			invertedAnim = false;
			animMoment = 0;
		}
		if (!invertedAnim) {
			animMoment += delta;
		} else {
			animMoment -= delta;
		}
		if (!path.hasNext()) {
			path.resetPath();
		}
		Vector2 currPosition = path.getNextPoint(delta * 20);
		setPosition(currPosition.x + xCurr, currPosition.y + yCurr);
		collideable.setPosition(currPosition.x + xCurr, currPosition.y + yCurr);
		animMoment += delta;
		if (animation.isAnimationFinished(animMoment)) {
			animMoment = 0;
		}
		setRegion(animation.getKeyFrame(animMoment));
	}
	/*public void draw(Batch batch) {
		super.draw(batch);
	}*/
	
	public int getPositionInPath() {
		return path.getCurrentIndexPoint(0);
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}

	@Override
	public ShapeCollideable getCollideable() {
		return collideable;
	}

	@Override
	public void substractLife(int life) throws NoLifeException{
		if (this.life - life < 1)
			throw new NoLifeException();
		this.life -= life;
	}

	@Override
	public int getDamage() {
		return life;
	}
	
}
