package com.example.m08.nausjoc.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class PlayerShipReinforcementsPathFactory {
	
	//private static final int TOTAL_DEGREES = 360;
	
	public static SpritePath getRotationalPath(float distanceFromCenter, int numPoints) {
		return getRotationalPath(0, 0, distanceFromCenter, numPoints);
	}
	
	public static SpritePath getRotationalPath(float x, float y, float distanceFromCenter, int numPoints) {
		if (numPoints <= 1) throw new IllegalArgumentException("Number of points must be superior of 1.");
		double time = distanceFromCenter * 2 * Math.PI;
		double velAngular = 2 * Math.PI / time;
		Array<Vector2> path = new Array<Vector2>();
		for (int i = 0; i <= numPoints; i++) {
			double momentAngular = ((double)i / numPoints) * (velAngular * time);
			float xPoint = (float) (distanceFromCenter * Math.cos(momentAngular) + x);
			float yPoint = (float) (distanceFromCenter * Math.sin(momentAngular) + y);
			Vector2 currentPoint = new Vector2(xPoint, yPoint);
			path.add(currentPoint);
		}
		SpritePath spritePath = new SpritePath(path);
		return spritePath;
	}
	
}
