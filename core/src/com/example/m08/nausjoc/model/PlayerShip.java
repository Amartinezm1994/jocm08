package com.example.m08.nausjoc.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.example.m08.nausjoc.shapeutils.CollideRectangle;
import com.example.m08.nausjoc.shapeutils.GroupedRectangleForms;
import com.example.m08.nausjoc.shapeutils.ShapeCollideable;

public class PlayerShip extends Sprite implements MovableShip, ICollisionHealtheable {
	
	private int life;
	private int reinforcementLife;
	//private List<Bullet> bullets;
	private BulletModel bulletModel;
	private BulletModel bulletModelReinforcement;
	private Animation animation;
	private Animation reinforcementAnimation;
	//private TextureRegion[] frames;
	private float animationMoment;
	private List<PlayerShipReinforcement> reinforcements;
	//private List<IC>
	private GroupedRectangleForms collisionRects;
	private boolean invertedAnim;
	private TextureRegion actualRegion;
	//private BulletPool bullPool;
	private BulletFactory factoryBullet;
	private int reinfBaseDamage;
	private float distanceFromCenter;
	
	private float maxHeight;
	private float maxWidth;
	
	private float differenceXLeft;
	private float differenceXRight;
	private float differenceYTop;
	private float differenceYBottom;
	
	private float velocity;
	
	private boolean leftMove;
	private boolean rightMove;
	private boolean upMove;
	private boolean downMove;
	
	private static final int DEFAULT_FLYING_POINTS_REINFORCEMENTS = 200;
	public static final int WIDTH_REINF = 50;
	public static final int HEIGHT_REINF = 80;
	
	public PlayerShip(Texture text, int x, int y, int width, int height, int life,
			int reinforcementLife/*, int reinfBaseDamage*/, float distanceFromCenter, 
			float velocity, Animation playerAnimation, 
			Animation reinforcementAnimation, GroupedRectangleForms collisionRects, 
			BulletModel bullModelShip, BulletModel bullModelReinf, 
			float maxWidth, float maxHeight) {
		super(text, x, y, width, height);
		this.life = life;
		this.animation = playerAnimation;
		this.reinforcementAnimation = reinforcementAnimation;
		this.velocity = velocity;
		this.maxHeight = maxHeight;
		this.maxWidth = maxWidth;
		this.reinforcementLife = reinforcementLife;
		//this.reinfBaseDamage = reinfBaseDamage;
		this.distanceFromCenter = distanceFromCenter;
		//frames = animation.getKeyFrames();
		invertedAnim = false;
		//bullPool = BulletPool.getInstance();
		factoryBullet = BulletFactory.getInstance();
		this.bulletModel = bullModelShip;
		this.bulletModelReinforcement = bullModelReinf;
		this.collisionRects = collisionRects;
		reinforcements = new ArrayList<PlayerShipReinforcement>();
		animationMoment = 0;
		leftMove = false;
		rightMove = false;
		upMove = false;
		downMove = false;
		differenceXLeft = 0;
		differenceXRight = 0;
		differenceYBottom = 0;
		differenceYTop = 0;
		//bullets = new ArrayList<Bullet>();
	}
	/**
	 * Set the padding in the sprite to collision with the screen
	 * limits.
	 * @param differenceXLeft padding left
	 * @param differenceXRight padding right
	 * @param differenceYTop padding top
	 * @param differenceYBottom padding bottom
	 */
	public void setDifferenceOfSprite(float differenceXLeft, float differenceXRight,
			float differenceYTop, float differenceYBottom) {
		this.differenceXLeft = differenceXLeft;
		this.differenceXRight = differenceXRight;
		this.differenceYBottom = differenceYBottom;
		this.differenceYTop = differenceYTop;
	}
	
	public void setPlayerBulletModel(BulletModel bulletModel) {
		this.bulletModel = bulletModel;
	}
	
	public List<Bullet> generateBullets() {
		List<Bullet> bullets = new ArrayList<Bullet>();
		
		if (reinforcements.size() > 0) {
			for (PlayerShipReinforcement reinf : reinforcements) {
				bullets.add(reinf.generateBullet());
			}
		}
		//System.out.println("creada");
		bullets.add(factoryBullet.getLinearBullet(bulletModel, (int)(getX() + getWidth() / 2), 
				(int)(getY() + getHeight()), BulletFactory.UP, 200, 10));
		/*bullets.add(bullPool.checkOut(bulletModel, 
				(int)(getX() + getWidth() / 2), (int)(getY() + getHeight())));*/
		//this.bullets.addAll(bullets);
		return bullets;
	}
	
	public void addNewReinforcement() {
		addNewReinforcement(reinfBaseDamage, distanceFromCenter);
	}
	public void addNewReinforcement(int baseDamage, float distanceFromCenter) {
		SpritePath path = PlayerShipReinforcementsPathFactory.getRotationalPath(distanceFromCenter, DEFAULT_FLYING_POINTS_REINFORCEMENTS);
		path.setCurrentPoint((int)getMiddlePointPosition());
		Vector2 currPoint = path.getCurrentPoint();
		ShapeCollideable collideable = new CollideRectangle(currPoint.x, 
				currPoint.y, WIDTH_REINF, HEIGHT_REINF);
		/*ShapeCollideable collideable = new CollideRectangle(new Rectangle(currPoint.x, currPoint.y,
				(float)bulletModelReinforcement.getWidth(), (float)bulletModelReinforcement.getHeight()));*/
		PlayerShipReinforcement reinforcement = new PlayerShipReinforcement(
				reinforcementLife/*, baseDamage*/, path, reinforcementAnimation, 
				WIDTH_REINF, HEIGHT_REINF, bulletModelReinforcement, collideable);
		//reinforcement.flip(true, true);
		reinforcement.rotate(180);
		reinforcements.add(reinforcement);
	}
	
	@Override
	public void draw(Batch batch) {
		
		
		
		//int keyFrameIndex = animation.getKeyFrameIndex(animationMoment);
		//animation.
		//TextureRegion actualRegion = frames[keyFrameIndex];
		//Texture txt = actualRegion.getTexture();
		//txt.getTextureData().
		//actualRegion.
		//batch.draw(.,0, 0);
		//set
		//new Texture(frames[keyFrameIndex].)
		//setTexture(frames[keyFrameIndex].getTexture());
		//System.out.println(getTexture());
		for (PlayerShipReinforcement reinf : reinforcements) {
			reinf.draw(batch);
		}
		//for (Bullet bull : bullets) {
		//	bull.draw(batch);
			//batch.draw(bull.getTexture(), 0, 0, 200, 200);
		//}
		super.draw(batch);
	}
	@Override
	public void setRegion(TextureRegion region) {
		actualRegion = region;
		super.setRegion(region);
	}
	public void update(float delta) {
		//ge
		TextureRegion auxRegion = animation.getKeyFrame(animationMoment);
		if (actualRegion != auxRegion) {
			setRegion(animation.getKeyFrame(animationMoment));
		}
		if (animation.isAnimationFinished(animationMoment)) {
			invertedAnim = true;
		} else if (animationMoment < 0) {
			invertedAnim = false;
			animationMoment = 0;
		}
		if (!invertedAnim) {
			animationMoment += delta;
		} else {
			animationMoment -= delta;
		}
		//if (getX() >= 0 && getY() >= 0 && getWidth() + getX() <= maxWidth 
		//		&& getHeight() + getY() <= maxWidth) {
			float pendingX = 0;
			float pendingY = 0;
			float thisVelocity = velocity * delta;
			if (leftMove) {
				pendingX -= thisVelocity;
			}
			if (rightMove) {
				pendingX += thisVelocity;
			}
			if (upMove) {
				pendingY += thisVelocity;
			}
			if (downMove) {
				pendingY -= thisVelocity;
			}
			if (pendingX != 0 || pendingY != 0) {
				move(pendingX, pendingY);
			}
			
		
		//}
		float xCurr = getX() + getWidth() / 2 - WIDTH_REINF / 2;
		float yCurr = getY() + getHeight() / 2 - HEIGHT_REINF / 2;
		Iterator<PlayerShipReinforcement> iterReinf = reinforcements.iterator();
		while (iterReinf.hasNext()) {
			PlayerShipReinforcement reinf = iterReinf.next();
			//if (reinf.getLife() < 1) {
			//	iterReinf.remove();
			//} else {
				reinf.update(delta, xCurr, yCurr);
			//}
		}
	}
	
	public void removeCollisionableHealtheable(ICollisionHealtheable collisionable) {
		reinforcements.remove(collisionable);
	}
	
	public void move(float x, float y) {
		if (!(getX() + differenceXLeft >= 0))
			leftMove = false;
		if (!(getWidth() + getX() - differenceXRight <= maxWidth))
			rightMove = false;
		if (!(getHeight() + getY() - differenceYTop <= maxHeight))
			upMove = false;
		if (!(getY() + differenceYBottom >= 0))
			downMove = false;
		setPosition(getX() + x, getY() + y);
		/*for (int i = 0; i < reinforcements.size; i++) {
			PlayerShipReinforcement reinf = reinforcements.get(i);
			reinf.setPosition(reinf.getX() + x, reinf.getY() + y);
			collisionRects.move(x, y);
		}*/
		//PolygonSprite sprite = new PolygonSprite(this);
		for (PlayerShipReinforcement reinf : reinforcements) {
			reinf.move(x, y);
		}
		collisionRects.setPosition(getX() + x + differenceXLeft, getY() + y + differenceYBottom);
	}
	
	public List<ICollisionHealtheable> getCollisionableHealtheables() {
		return new ArrayList<ICollisionHealtheable>(reinforcements);
	}
	

	@Override
	public void setLeftMove(boolean leftMove) {
		if (getX() >= 0)
			this.leftMove = leftMove;
		//else
		//	this.leftMove = false;
	}
	
	@Override
	public void setRightMove(boolean rightMove) {
		if (getWidth() + getX() <= maxWidth)
			this.rightMove = rightMove;
		//else
		//	this.rightMove = false;
	}
	
	@Override
	public void setUpMove(boolean upMove) {
		if (getHeight() + getY() <= maxHeight)
			this.upMove = upMove;
		//else
		//	this.upMove = false;
	}
	
	@Override
	public void setDownMove(boolean downMove) {
		if (getY() >= 0)
			this.downMove = downMove;
		//else
		//	this.downMove = false;
	}
	
	public void setCollisionLimits(float width, float height) {
		//Intersector.in
		this.maxHeight = height;
		this.maxWidth = width;
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}
	@Override
	public ShapeCollideable getCollideable() {
		return collisionRects;
	}
	
	
	public int getNumReinforcements() {
		return reinforcements.size();
	}
	
	private float getPartitionSize(int length) {
		if ((length & -length) != length)
			return getPartitionSize(--length);
		return length * 2;
	}
	private float getMiddlePointPosition() {
		if (reinforcements.size() == 0) {
			return 0;
		} else if (reinforcements.size() == 1) {
			float partitionSize = DEFAULT_FLYING_POINTS_REINFORCEMENTS / 2;
			float positionInPath = reinforcements.get(0).getPositionInPath();
			if (positionInPath >= partitionSize) {
				return positionInPath - partitionSize;
			}
			return positionInPath + partitionSize;
		} else {
			float partitionSize = DEFAULT_FLYING_POINTS_REINFORCEMENTS / getPartitionSize(reinforcements.size());
			float[] points = new float[reinforcements.size()];
			for (int i = 0; i < points.length; i++) {
				points[i] = reinforcements.get(i).getPositionInPath();
			}
			for (int i = 0; i < points.length; i++) {
				for (int j = i + 1; j < points.length; j++) {
					if (points[i] > points[j]) {
						float auxPoint = points[i];
						points[i] = points[j];
						points[j] = auxPoint;
					}
				}
			}
			for (int i = 0; i < points.length - 1; i++) {
				if (points[i + 1] - points[i] > partitionSize) {
					return points[i] + partitionSize;
				}
			}
			return points[points.length - 1] + partitionSize;
		}
	}
	@Override
	public void substractLife(int life) throws NoLifeException {
		if (this.life - life < 1)
			throw new NoLifeException();
		this.life -= life;
	}
	@Override
	public int getDamage() {
		return life;
	}
}
