package com.example.m08.nausjoc.model;

public interface MovableShip {
	public void setLeftMove(boolean leftMove);
	public void setRightMove(boolean rightMove);
	public void setUpMove(boolean upMove);
	public void setDownMove(boolean downMove);
}
