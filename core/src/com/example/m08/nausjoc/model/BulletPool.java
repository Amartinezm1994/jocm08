package com.example.m08.nausjoc.model;

import java.util.Enumeration;
import java.util.Hashtable;

import com.badlogic.gdx.graphics.Texture;
import com.example.m08.nausjoc.shapeutils.ShapeCollideable;

public class BulletPool {

	public static final long DEFAULT_EXPIRATION_TIME = 5000l;
	
	private long expireTime;
	private Hashtable<Bullet, Long> locked, unlocked;
	//private int screenWidth;
	//private int screenHeight;
	
	private static BulletPool INSTANCE = null;
	
	private BulletPool(long expireTime) {
		this.expireTime = expireTime;
		locked = new Hashtable<Bullet, Long>();
		unlocked = new Hashtable<Bullet, Long>();
	}
	
	public static BulletPool getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new BulletPool(DEFAULT_EXPIRATION_TIME);
		}
		return INSTANCE;
	}
	/*public BulletPool() {
		this(DEFAULT_EXPIRATION_TIME);
	}*/
	
	/*public Bullet create(BulletModel model, int x, int y) {
		return new Bullet(model, x, y);
	}*/
	public Bullet create(Texture text, 
			int x, int y, int width, int height, int damage, ShapeCollideable collideable) {
		return new Bullet(new BulletModel(text, damage, width, height, collideable.getType()), x, y, collideable);
	}
	
	public void validate(Bullet bullet, Texture text, 
			int x, int y, int width, int height, int damage,
			ShapeCollideable collideable) {
		bullet.setTexture(text);
		bullet.setRegion(x, y, width, height);
		bullet.setX(x);
		bullet.setY(y);
		//bullet.set
		bullet.setDamage(damage);
		bullet.setCollideable(collideable);
		bullet.setSize(Math.abs(width), Math.abs(height));
		bullet.setOrigin(width / 2, height / 2);
		//bullet.setPath(null);
	}
	public void expire(Bullet bullet) {
		//TODO
		bullet.setTexture(null);
		bullet.setPath(null);
		bullet.setCollideable(null);
		bullet.setDamage(0);
		//bullet.setCollideable(null);
		//bullet.setR
		//bullet.setRegion();
		//bullet.set
		//bullet.setCollideable(null);
		//bullet.
	}
	
	public synchronized Bullet checkOut(Texture text, 
			int x, int y, int width, int height, int damage,
			ShapeCollideable collideable) {
		//System.out.println("Unlocked size: " + unlocked.size() + " locked size: " + locked.size());
		if (unlocked.size() > 0) {
			Enumeration<Bullet> enumer = unlocked.keys();
			while(enumer.hasMoreElements()) {
				Bullet obj = enumer.nextElement();
				if (now() - unlocked.get(obj) > expireTime) {
					unlocked.remove(obj);
					expire(obj);
				} else {
					validate(obj, text, x, y, width, height, damage, collideable);
					locked.put(obj, now());
					unlocked.remove(obj);
					return obj;
				}
			}
		}
		Bullet bull = create(text, x, y, width, height, damage, collideable);
		locked.put(bull, now());
		return bull;
	}
	public synchronized Bullet checkOut(BulletModel model, int x, int y, 
			ShapeCollideable collideable) {
		return checkOut(model.getTexture(), x, y, model.getWidth(), 
				model.getHeight(), model.getDamage(), collideable);
	}
	
	public synchronized void checkIn(Bullet object) {
		locked.remove(object);
		unlocked.put(object, now());
	}
	
	public static long now() {
		return System.currentTimeMillis();
	}
}
