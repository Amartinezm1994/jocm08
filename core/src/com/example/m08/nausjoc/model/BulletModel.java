package com.example.m08.nausjoc.model;

import com.badlogic.gdx.graphics.Texture;
import com.example.m08.nausjoc.shapeutils.ShapeType;

public class BulletModel {
	
	private Texture texture;
	//private ShapeCollideable collideable;
	private int damage;
	private int width, height;
	private ShapeType typeBullet;
	
	public BulletModel(Texture text, int damage, int width, int height, ShapeType typeBullet) {
		this.texture = text;
		this.damage = damage;
		this.width = width;
		this.height = height;
		this.typeBullet = typeBullet;
		//this.collideable = collideable;
	}

	public Texture getTexture() {
		return texture;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public ShapeType getTypeBullet() {
		return typeBullet;
	}

	public void setTypeBullet(ShapeType typeBullet) {
		this.typeBullet = typeBullet;
	}

	/*public ShapeCollideable getCollideable() {
		return collideable;
	}*/

	/*public void setCollideable(ShapeCollideable collideable) {
		this.collideable = collideable;
	}*/
	
}
